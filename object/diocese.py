# -*- coding: utf-8 -*-
from openerp.osv import osv, fields





class diocese_eglise(osv.Model):
    _inherit = 'res.company'   
   
    def write(self, cr, uid, ids, vals, context=None):
        v_name = None
        v_firstname = None

        if vals.get('name'):
            vals['name'] = vals['name'].title() 
            
        result = super(diocese_eglise,self).write(cr, uid, ids, vals, context=context)
        return result

    def create(self, cr, uid, vals, context=None):
        v_name = None
        v_firstname = None

        if vals.get('name'):
            vals['name'] = vals['name'].title()   
        
        result = super(diocese_eglise,self).create(cr, uid, vals, context=context)
        return result 
    _columns = {
            'responsable':fields.many2one('res.partner','Responsable eglise',domain=[('is_engagement_religieux','=',True)]),
            
            'evec_id' :fields.many2one('paroisse.pretre','Eveque',domain="[('civilite_pretre','in',['eveque'])]"),
            'curee_id' :fields.many2one('paroisse.pretre','Curé',domain="[('civilite_pretre','in',['abbe','pere'])]"),
            'is_cathedrale':fields.boolean('Cathédrale ?'),
            'is_diocese':fields.boolean('Diocèse ?'),
            'archevec_id' :fields.many2one('paroisse.pretre','Archeveque',domain="[('civilite_pretre','in',['archeveque','cardinal'])]"),
            'commune_id': fields.many2one('res.communes', string='Commune'),   
            'company_id': fields.many2one('res.company', string='Company'),                       
            'doyenne_id': fields.many2one('res.doyenne', string='Doyenné de'),                       


} 
    
diocese_eglise()