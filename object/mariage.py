# -*- coding: utf-8 -*-
from openerp.osv import osv, fields

class paroisse_mariage(osv.osv):
    _name = 'paroisse.mariage'
    _description = 'Mariage'
    
    def _get_test_f_deces(self, cr, uid, ids,field, arg, context=None):
        res = {} 
        records = self.browse(cr,uid,ids,context)       
        for record in records:          
            res[record['id']] = record.marie_id.date_deces  and True or False
        return res
    
    def _get_test_m_deces(self, cr, uid, ids,field, arg, context=None):
        res = {} 
        records = self.browse(cr,uid,ids,context)       
        for record in records:          
            res[record['id']] = record.mari_id.date_deces  and True or False
        return res
    
    
    def write(self, cr, uid, ids, vals, context=None):
        v_name = None
        v_firstname = None

        if vals.get('prenom_temoin1'):
            v_firstname = vals['prenom_temoin1'].strip()
            vals['prenom_temoin1'] = v_firstname.title()
        if vals.get('nom_temoin1'):
            v_firstname = vals['nom_temoin1'].strip()
            vals['nom_temoin1'] = v_firstname.title()
            
        if vals.get('prenom_temoin2'):
            v_firstname = vals['prenom_temoin2'].strip()
            vals['prenom_temoin2'] = v_firstname.title()
        if vals.get('nom_temoin2'):
            v_firstname = vals['nom_temoin2'].strip()
            vals['nom_temoin2'] = v_firstname.title()
        
        if vals.get('prenom_temoin3'):
           v_firstname = vals['prenom_temoin3'].strip()
           vals['prenom_temoin3'] = v_firstname.title() 
        if vals.get('nom_temoin3'):
           v_firstname = vals['nom_temoin3'].strip()
           vals['nom_temoin3'] = v_firstname.title()  
        
        if vals.get('prenom_temoin4'):
            v_firstname = vals['prenom_temoin4'].strip()
            vals['prenom_temoin4'] = v_firstname.title() 
        if vals.get('nom_temoin4'):
            v_firstname = vals['nom_temoin4'].strip()
            vals['nom_temoin4'] = v_firstname.title()  
            
    ##    if vals.get('parent_comp_id'):
     ##       v_firstname = vals['parent_comp_id'].strip()
      ##      vals['parent_comp_id'] = v_firstname.title()
         
        if vals.get('pretre_id'):
            vals['pretre_id'] = vals['pretre_id'].title()        
         
        if vals.get('marie_id',False):
            cr.execute("update res_partner set is_maried ='t' and civ_religieuse ='mariage' where id =%s"% vals.get('marie_id'))
        if vals.get('mari_id',False):
            cr.execute("update res_partner set is_maried ='t' and civ_religieuse ='mariage' where id =%s"% vals.get('mari_id'))
        
        result = super(paroisse_mariage,self).write(cr, uid, ids, vals, context=context)
        return result

    def create(self, cr, uid, vals, context=None):
        v_name = None
        v_firstname = None
        
        if vals.get('prenom_temoin1'):
            v_firstname = vals['prenom_temoin1'].strip()
            vals['prenom_temoin1'] = v_firstname.title()
        if vals.get('nom_temoin1'):
            v_firstname = vals['nom_temoin1'].strip()
            vals['nom_temoin1'] = v_firstname.title()
            
        if vals.get('prenom_temoin2'):
            v_firstname = vals['prenom_temoin2'].strip()
            vals['prenom_temoin2'] = v_firstname.title()
        if vals.get('nom_temoin2'):
            v_firstname = vals['nom_temoin2'].strip()
            vals['nom_temoin2'] = v_firstname.title()
        
        if vals.get('prenom_temoin3'):
           v_firstname = vals['prenom_temoin3'].strip()
           vals['prenom_temoin3'] = v_firstname.title() 
        if vals.get('nom_temoin3'):
           v_firstname = vals['nom_temoin3'].strip()
           vals['nom_temoin3'] = v_firstname.title()  
        
        if vals.get('prenom_temoin4'):
            v_firstname = vals['prenom_temoin4'].strip()
            vals['prenom_temoin4'] = v_firstname.title() 
        if vals.get('nom_temoin4'):
            v_firstname = vals['nom_temoin4'].strip()
            vals['nom_temoin4'] = v_firstname.title()  
            
            
##        if vals.get('parent_comp_id'):
  ##          vals['parent_comp_id'] = vals['parent_comp_id'].title()
            
        if vals.get('pretre_id'):
            vals['pretre_id'] = vals['pretre_id'].title()    

        result = super(paroisse_mariage,self).create(cr, uid, vals, context=context)
        return result
    
    def unlink(self,cr,uid,id,context):  
       # 
        mariage =self.browse(cr,uid,id,context)
        cr.execute("update res_partner set is_maried ='f' and civ_religieuse ='prof_foi' where id in "+str((mariage.mari_id.id,mariage.marie_id.id)))
        return super(paroisse_mariage,self).unlink(cr,uid,id,context)
                       
    
    _order = 'date_mariage desc'
    _columns = {
            'name' :fields.integer('Numéro mariage',required=True),
            'date_mariage': fields.date('Date mariage',required=True),
            'mari_id':fields.many2one('res.partner','Marié', required=True, domain="['&',('is_paroissien','=',True),('sexe','=','m'),('is_maried','=',False),('civ_religieuse','!=','decede')]"),
            'marie_id':fields.many2one('res.partner','Mariée', required=True, domain="['&',('is_paroissien','=',True),('sexe','=','f'),('is_maried','=',False),('civ_religieuse','!=','decede')]"),
            'date_bapteme_mari':fields.date('Date baptème', ),
            'date_naissance':fields.date('Date naissance', ),
            'nom_marie':fields.char('Nom mari', ),
            'pere_mari':fields.char('Nom du père', ),
            'mere_mari':fields.char('Nom de la mère', ),
            'pretre_id':fields.char('Célébré par'),
            'temoin1' :fields.char('Temoin 1',required=False),
            'temoin2' :fields.char('Temoin 2',required=False),
            'temoin3' :fields.char('Temoin 3',required=False),
            'temoin4' :fields.char('Temoin 4',required=False),
            
            'prenom_temoin1' :fields.char('Prenom Témoin 1',required=True),
            'nom_temoin1' :fields.char('Nom',required=True),
            'prenom_temoin2' :fields.char('Prénom Temoin 2',required=True),
            'nom_temoin2' :fields.char('Nom',required=True),
            'prenom_temoin3' :fields.char('Prénom Temoin 3',required=False),
            'nom_temoin3' :fields.char('Nom',required=False),
            'prenom_temoin4' :fields.char('Prénom Temoin 4',required=False),
            'nom_temoin4' :fields.char('Nom',required=False),
     
            
            'actif':fields.boolean('Active'),
            'pretre_id':fields.char('Célébré par',required=True),
            'comp_id':fields.many2one('res.company','Eglise', required=True),
            'parent_comp_id':fields.many2one('res.company','Diocèse', required=True),
            
            'is_f_decede': fields.function(_get_test_f_deces, method=True,
            type='boolean', string='Décès',),
            'is_m_decede': fields.function(_get_test_m_deces, method=True,
            type='boolean', string='Décès',),

           # 'is_m_decede':fields.function()

            
 } 
    
    def create_mariage(self, cr, uid, ids, context={}):
         values ={}
         values['civ_religieuse']='mariage'
         values['is_maried']=True
        # super(paroisse_mariage, self).create(cr,uid,id)
         wizard = self.browse(cr,uid,ids[0],context)
         partner_obj = self.pool.get("res.partner")
         mariage_valuse = {
             
            'numero_mariage':  wizard.name ,           
            'date_mariage': wizard.date_mariage  ,
            'mari_id':wizard.mari_id.id  ,
            'marie_id':wizard.marie_id.id  ,
            'date_bapteme_mari':wizard.date_bapteme_mari  ,
            #'date_naissance':wizard.date_naissance  ,
            'nom_marie':wizard.nom_marie  ,
            'pere_mari':wizard.pere_mari  ,
            'mere_mari':wizard.mere_mari  ,
            'pretre_id':wizard.pretre_id  ,
   #         'temoin1' :wizard.temoin1  ,
  #          'temoin2' :wizard.temoin2  ,
   #         'temoin3' :wizard.temoin3  ,
    #        'temoin4' :wizard.temoin4  ,
            'prenom_temoin1':wizard.prenom_temoin1 ,
            'nom_temoin1':wizard.nom_temoin1,
            'prenom_temoin2':wizard.prenom_temoin2 ,
            'nom_temoin2':wizard.nom_temoin2,
            'prenom_temoin3':wizard.prenom_temoin3 ,
            'nom_temoin3':wizard.nom_temoin3,
            'prenom_temoin4':wizard.prenom_temoin4 ,
            'nom_temoin4':wizard.nom_temoin4,
            
            'actif':wizard.actif,
            'pretre_id':wizard.pretre_id  ,
            'comp_id':wizard.comp_id.id  ,
            'parent_comp_id':wizard.parent_comp_id.id,
            'is_maried':True,
            'civ_religieuse':'mariage'
            
            }
         
         partner_obj.write(cr, uid, wizard.mari_id.id, mariage_valuse, context={})
         partner_obj.write(cr, uid, wizard.marie_id.id, mariage_valuse, context={})
        
         self.pool.get("res.partner").write(cr,uid,wizard.mari_id.id,values)  
         self.pool.get("res.partner").write(cr,uid,wizard.marie_id.id,values) 
        
         return True
    
    _defaults = {  
        'actif': True,
  #      'comp_id': lambda self, cr, uid, ctx: self.pool.get('res.company')._company_default_get(cr, uid, 'res.partner', context=ctx),

        }
paroisse_mariage()