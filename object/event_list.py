# -*- coding: utf-8 -*-
#----------------------------------------------------------------------------
#
#    Copyright (C) 2014  .
#    TeamIT
#
#----------------------------------------------------------------------------
from openerp.osv import osv
from openerp.report import report_sxw
import datetime 

from datetime import date

#-------------------------------------------------------------------------------
# journal_salary_report
#-------------------------------------------------------------------------------
class event_list(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(event_list, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
        'get_all_line' : self._get_all_line,
         
        })
    
    def _get_all_line(self):
        event_obj=self.pool.get('event.event')
        monday = (date.today() - datetime.timedelta(days=date.today().weekday())).strftime('%Y-%m-%d')
        print monday
        sunday = (date.today() - datetime.timedelta(days=date.today().weekday()) + datetime.timedelta(days=6)).strftime('%Y-%m-%d')
        print sunday
        search = event_obj.search(self.cr,self.uid,['&',('date_begin','<=',sunday),('date_begin','>=',monday)])
        print search
        all_line = event_obj.browse(self.cr,self.uid,search)
        print '---------------------------all_line',all_line
        return all_line
    
class report_event_list(osv.AbstractModel):
    _name = 'report.gestion_paroisse.event_list'
    _inherit = 'report.abstract_report'
    _template = 'gestion_paroisse.event_list'
    _wrapped_report_class = event_list 