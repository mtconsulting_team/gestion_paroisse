# -*- encoding: utf-8 -*-

from openerp.osv import osv, fields
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import _


class recette_paroisse(osv.osv):
    _inherit = 'gestion.paroisse.depense.recette'

    # override list in custom module to add/drop columns or change order
    def _report_xls_fields(self, cr, uid, context=None):
        return [
            'description','user_id','event_id','debit', 'credit', 'type_event_id', 'date_dep'
        ]

    def _report_xls_template(self, cr, uid, context=None):
        """
        Template updates, e.g.

        my_change = {
            'move':{
                'header': [1, 20, 'text', _('My Move Title')],
                'lines': [1, 0, 'text', _render("line.move_id.name or ''")],
                'totals': [1, 0, 'text', None]},
        }
        return my_change
        """
        return {}
    
    _columns = {

                }