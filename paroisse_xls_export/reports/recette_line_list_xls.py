# -*- encoding: utf-8 -*-
import xlwt
import time
from datetime import datetime
from openerp.osv import orm
from openerp.report import report_sxw
from openerp.addons.report_xls.report_xls import report_xls
from openerp.addons.report_xls.utils import rowcol_to_cell, _render
from openerp.tools.translate import translate, _
from openerp import pooler
import logging
_logger = logging.getLogger(__name__)

_ir_translation_name = 'recette.line.list.xls'


class recette_line_xls_parser(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(recette_line_xls_parser, self).__init__(cr, uid, name, context=context)
        move_obj = self.pool.get('gestion.paroisse.depense.recette')
        self.context = context
        wanted_list = move_obj._report_xls_fields(cr, uid, context)
        template_changes = move_obj._report_xls_template(cr, uid, context)
        self.localcontext.update({
            'datetime': datetime,
            'wanted_list': wanted_list,
            'template_changes': template_changes,
            '_': self._,
        })

    def _(self, src):
        lang = self.context.get('lang', 'en_US')
        return translate(self.cr, _ir_translation_name, 'report', lang, src) or src


class voucher_line_xls(report_xls):

    def __init__(self, name, table, rml=False, parser=False, header=True, store=False):
        super(voucher_line_xls, self).__init__(name, table, rml, parser, header, store)

        # Cell Styles
        _xs = self.xls_styles
        # header
        rh_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rh_cell_style = xlwt.easyxf(rh_cell_format)
        self.rh_cell_style_center = xlwt.easyxf(rh_cell_format + _xs['center'])
        self.rh_cell_style_right = xlwt.easyxf(rh_cell_format + _xs['right'])
        # lines
        aml_cell_format = _xs['borders_all']
        self.aml_cell_style = xlwt.easyxf(aml_cell_format)
        self.aml_cell_style_center = xlwt.easyxf(aml_cell_format + _xs['center'])
        self.aml_cell_style_date = xlwt.easyxf(aml_cell_format + _xs['left'], num_format_str=report_xls.date_format)
        self.aml_cell_style_decimal = xlwt.easyxf(aml_cell_format + _xs['right'], num_format_str=report_xls.decimal_format)
        # totals
        rt_cell_format = _xs['bold'] + _xs['fill'] + _xs['borders_all']
        self.rt_cell_style = xlwt.easyxf(rt_cell_format)
        self.rt_cell_style_right = xlwt.easyxf(rt_cell_format + _xs['right'])
        self.rt_cell_style_decimal = xlwt.easyxf(rt_cell_format + _xs['right'], num_format_str=report_xls.decimal_format)

        # XLS Template
        self.col_specs_template = {
                                   
            'description': {
                'header': [1, 25, 'text', _render("_('Description')")],
                'lines': [1, 0, 'text', _render("line.description or ''")],
                'totals': [1, 0, 'text', None]},
            'user_id': {
                'header': [1, 25, 'text', _render("_('Utilisateur')")],
                'lines': [1, 0, 'text', _render("line.user_id.name or ''")],
                'totals': [1, 0, 'text', None]},
            'event_id': {
                'header': [1, 25, 'text', _render("_('Evenement')")],
                'lines': [1, 0, 'text', _render("line.event_id.name or ''")],
                'totals': [1, 0, 'text', None]},
            'debit': {
                'header': [1, 25, 'text', _render("_('Recette')")],
                'lines': [1, 0, 'number', _render("line.debit or ''")],
                'totals': [1, 0, 'number', None, _render("montant_formula"), self.rt_cell_style_decimal]},

            'credit': {
                'header': [1, 25, 'text', _render("_('Depense')")],
                'lines': [1, 0, 'number', _render("line.credit or ''")],
                'totals': [1, 0, 'number', None, _render("montant_formula_credit"), self.rt_cell_style_decimal]},
                #'totals': [1, 0, 'text', None]},

             'type_event_id': {
                'header': [1, 25, 'text', _render("_('Type Evenement')")],
                'lines': [1, 0, 'text', _render("line.type_event_id.name or ''")],
                # 'totals': [1, 0, 'number', None, _render("montant_formula"), self.rt_cell_style_decimal]},
                'totals': [1, 0, 'text', None]},

            'date_dep': {
                'header': [1, 13, 'text', _render("_('Date')")],
                'lines': [1, 0, 'date', _render("datetime.strptime(line.date_dep,'%Y-%m-%d %H:%M:%S')"), None, self.aml_cell_style_date],
                'totals': [1, 0, 'text', None]},                           

        }                                 
#            

    def generate_xls_report(self, _p, _xs, data, objects, wb):

        wanted_list = _p.wanted_list
        self.col_specs_template.update(_p.template_changes)
        _ = _p._
        
        montant_pos = 'debit' in wanted_list and wanted_list.index('debit')
        montant_credit = 'credit' in wanted_list and wanted_list.index('credit')
        report_name = _("Relevé de recette encaissées sur occupation de la voie publique")
        ws = wb.add_sheet(report_name[:31])
        ws.panes_frozen = True
        ws.remove_splits = True
        ws.portrait = 0  # Landscape
        ws.fit_width_to_pages = 1
        row_pos = 0
        # set print header/footer
        ws.header_str = self.xls_headers['standard']
        ws.footer_str = self.xls_footers['standard']

        # Title
        cell_style = xlwt.easyxf(_xs['xls_title'])
        c_specs = [
            ('report_name', 1, 0, 'text', report_name),
        ]
        row_data = self.xls_row_template(c_specs, ['report_name'])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=cell_style)
        row_pos += 1
        # Column headers
        c_specs = map(lambda x: self.render(x, self.col_specs_template, 'header', render_space={'_': _p._}), wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=self.rh_cell_style, set_column_size=True)
        ws.set_horz_split_pos(row_pos)
        # account voucher lines
        for line in objects:
            c_specs = map(lambda x: self.render(x, self.col_specs_template, 'lines'), wanted_list)
            row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
            print ws ,"------------------",row_pos , "----------------------", row_data
            row_pos = self.xls_write_row(ws,row_pos, row_data, row_style=self.aml_cell_style)
        # Totals
        aml_cnt = len(objects)
        montant_start = rowcol_to_cell(row_pos - aml_cnt, montant_pos)
        montant_start_credit = rowcol_to_cell(row_pos - aml_cnt, montant_credit)
        print aml_cnt,'--------------',montant_pos,'--------------',montant_start
        montant_stop = rowcol_to_cell(row_pos - 1, montant_pos)
        montant_stop_credit = rowcol_to_cell(row_pos - 1, montant_credit)
        print montant_stop
        montant_formula = 'SUM(%s:%s)' % (montant_start, montant_stop)
        montant_formula_credit = 'SUM(%s:%s)' % (montant_start_credit, montant_stop_credit)
        print montant_formula
        c_specs = map(lambda x: self.render(x, self.col_specs_template, 'totals'), wanted_list)
        row_data = self.xls_row_template(c_specs, [x[0] for x in c_specs])
        row_pos = self.xls_write_row(ws, row_pos, row_data, row_style=self.rt_cell_style_right)

voucher_line_xls('report.recette.line.list.xls',
    'gestion.paroisse.depense.recette',
    parser=recette_line_xls_parser)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
