# -*- coding: utf-8 -*-
import time
from datetime import date
from datetime import datetime
from datetime import timedelta
from dateutil import relativedelta
from openerp.tools import DEFAULT_SERVER_DATETIME_FORMAT
from openerp import api, tools
from openerp.osv import fields, osv
from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp
from openerp.tools.safe_eval import safe_eval as eval
from openerp.exceptions import except_orm

class report_xls_recettes(osv.osv_memory):
    _name = "report.xls.recettes"
    _description = "Export des recettes"
    
    
    def abn_open_window(self, cr, uid, ids, context=None):
        mod_obj = self.pool.get('ir.model.data')
        act_obj = self.pool.get('ir.actions.act_window')
        if context is None:
            context = {}
        data = self.read(cr, uid, ids, context=context)[0]
        result = mod_obj.get_object_reference(cr, uid, 'gestion_paroisse', 'action_recette_paroisse')
        id = result and result[1] or False
        result = act_obj.read(cr, uid, [id], context=context)[0]
        if (data['date_begin']<=data['date_end']):
            date_from = datetime.strptime(data['date_begin'],'%Y-%m-%d')
            print date_from
            date_to = datetime.strptime(data['date_end'],'%Y-%m-%d')
            result['context'] = str({'search_default_recette_date_from':date_from, 'search_default_recette_date_to':date_to})
        else:
            raise except_orm(_('Warning!'), _('La date de fin est inférieure à la date de début.'))
        print '------------------------',result
        return result
    
    
    _columns = {
        'date_begin': fields.date('Date début', required=True),
        'date_end': fields.date('Date fin', required=True),
                }