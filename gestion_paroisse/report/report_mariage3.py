from openerp.osv import osv, fields
from openerp import tools
from openerp.report import report_sxw
from openerp.addons.account.report.common_report_header import common_report_header

class etat_mariage3(report_sxw.rml_parse, common_report_header):

   def __init__(self, cr, uid, name, context=None):
        super(etat_mariage3, self).__init__(cr, uid, name, context=context)
        # self.montant=0.0
        self.localcontext.update({
            # 'lines': self.lines,
            # 'get_start_date': self.get_start_date,
            # 'get_end_date': self.get_end_date,
            # 'get_somme': self.get_somme,
            # 'zone_je_soussigne': self.fonction_transfert,
        })
        self.context = context

   # def _get_personnalisation(self, cr, uid, ids, name, arg, context=None):
   #      res = {}
   #      info_obj = self.pool.get('paroisse.je_soussigne')
   #      records = info_obj.browse(cr,uid,ids, context=context)
        # res['zone_je_soussigne'] = records.zone_je_soussigne
        # res['nom_paroisse'] = records.nom_paroisse

        # return {'value':{'zone_je_soussigne': records.zone_je_soussigne}}


class report_mariage3_id(osv.AbstractModel):
    _name = 'report.gestion_paroisse.report_mariage3_id'
    _inherit = 'report.abstract_report'
    _template = 'gestion_paroisse.report_mariage3_id'
    _wrapped_report_class = etat_mariage3