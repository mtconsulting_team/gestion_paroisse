from openerp.osv import osv, fields
from openerp import tools
from openerp.report import report_sxw
from openerp.addons.account.report.common_report_header import common_report_header
# import convertion

# class raport_abonnement_impayee(report_sxw.rml_parse, common_report_header):
#     _name = 'report.abonnement.impaye'

class etat_mariage2(report_sxw.rml_parse, common_report_header):
   # _name = 'etat.mariage2'

   # def fonction_transfert(self, cr,uid, ids,name,arg, context=None):
   def fonction_transfert(self):
       return "good"
         # res = {}
         # res['nom_paroisse'] = 'OK'
         # return res


   def __init__(self, cr, uid, name, context=None):
        super(etat_mariage2, self).__init__(cr, uid, name, context=context)
        # self.montant=0.0
        self.localcontext.update({
            # 'lines': self.lines,
            # 'get_start_date': self.get_start_date,
            # 'get_end_date': self.get_end_date,
            # 'get_somme': self.get_somme,
            'zone_je_soussigne': self.fonction_transfert,
        })
        self.context = context

   def _get_personnalisation(self, cr, uid, ids, name, arg, context=None):
        res = {}
        info_obj = self.pool.get('paroisse.je_soussigne')
        records = info_obj.browse(cr,uid,ids, context=context)
        # res['zone_je_soussigne'] = records.zone_je_soussigne
        # res['nom_paroisse'] = records.nom_paroisse

        return {'value':{'zone_je_soussigne': records.zone_je_soussigne}}


   def fonction_transfert2(self, cr,uid, ids,name,arg, context=None):
         res = {}
         zone_obj = self.pool.get('paroisse.je_soussigne')
         zones = zone_obj.browse(cr, uid, ids, context=context)
         for zone in zones:
            res[zone.id] = zone.nom_paroisse
             #    return {'value':{'zone_residence': zone.zone_residence.id}}
         # return {'value': res}
         # return {'value':{'name':zone.nom_paroisse2}}
         return res


# class raport_abonnement_impaye(osv.AbstractModel):
#     _name = 'report.gestion_mairie.raport_abonnement_impaye'
#     _inherit = 'report.abstract_report'
#     _template = 'gestion_mairie.raport_abonnement_impaye'
#     _wrapped_report_class = raport_abonnement_impayee

class report_mariage2_id(osv.AbstractModel):
    _name = 'report.gestion_paroisse.report_mariage2_id'
    _inherit = 'report.abstract_report'
    _template = 'gestion_paroisse.report_mariage2_id'
    _wrapped_report_class = etat_mariage2