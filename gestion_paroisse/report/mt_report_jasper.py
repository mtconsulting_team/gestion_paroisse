# -*- encoding: utf-8 -*-

from openerp.addons.jasper_reports import jasper_report
import openerp.pooler
import time, datetime
import base64
import os

def generate_records( cr, uid, ids, data,context):
        return {
            'type'          : 'ir.actions.report.xml',
            'report_name'   : 'mariage_report',
            'datas': {
                    'model':'paroisse.mariage',
                    'id': context.get('active_ids') and context.get('active_ids')[0] or False,
                    'ids': context.get('active_ids') and context.get('active_ids') or [],
                    'parameters':data,
                  
                },
            'nodestroy': True
            }

jasper_report.report_jasper('report.mariage_report',model='paroisse.mariage',parser=generate_records)
  

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
