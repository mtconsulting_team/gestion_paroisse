# -*- coding: utf-8 -*-
from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime
import time
import openerp.addons

class type_recette(osv.osv):
    _name = "gestion.paroisse.recette"
    _description = "Recette paroisse"
    
    
    
    # Moi
     
    def write(self, cr, uid, ids, vals, context=None):
        v_name = None
        v_firstname = None

        if vals.get('name'):
            v_firstname = vals['name'].strip()
            vals['name'] = v_firstname.title()
                          
        result = super(type_recette,self).write(cr, uid, ids, vals, context=context)
        return result

    def create(self, cr, uid, vals, context=None):
        v_name = None
        v_firstname = None
        
        if vals.get('name'):
            v_firstname = vals['name'].strip()
            vals['name'] = v_firstname.title()
       
        result = super(type_recette,self).create(cr, uid, vals, context=context)
        return result
           
    #Moi
    
    _columns = {
                'name':fields.char('Recettes paroisse'),
                }

class recette( osv.osv ):   
    _inherit = "gestion.paroisse.depense.recette"
    _description = "Recette paroisse"
    _rec_name = 'event_id'  
    
    
    def _get_balance(self, cr, uid, ids,field, arg, context=None):
        res = {} 
        reads = self.read(cr, uid, ids, ['debit','credit'], context=context)        
        for record in reads:
            debit = record['debit']
            credit = record['credit']                   
            res[record['id']] = debit - credit
        return res
    
# Moi
     
    def write(self, cr, uid, ids, vals, context=None):
        v_name = None
        v_firstname = None

        if vals.get('description'):
            v_firstname = vals['description'].strip()
            vals['description'] = v_firstname.title()
                          
        result = super(recette,self).write(cr, uid, ids, vals, context=context)
        return result

    def create(self, cr, uid, vals, context=None):
        if context is None:
            context = {}
        data_obj = self.pool.get('ir.model.data')
        sequence_ids = data_obj.search(cr, uid, [('name','=','seq_recette_paroisse')], context=context)
        sequence_id = data_obj.browse(cr, uid, sequence_ids[0], context=context).res_id
        code = self.pool.get('ir.sequence').get_id(cr, uid, sequence_id, 'id', context) or '/'
        vals.update({'name': code})
        return super(recette, self).create(cr, uid, vals, context=context)
           
    #Moi
    
    _columns = {
        'name':fields.char('Recette No', size=64, readonly=True),
        'description':fields.char('Description', size=64, required=True),
        'type_recettes':fields.many2one('gestion.paroisse.recette', 'Type de Recettes', required=False), 
        'debit':fields.integer('Recette'),
        'balance':fields.function(_get_balance,
        method=True, type='float', string='Bilan', store=True),
        'recette_date_from':fields.function(lambda *a,**k:{}, method=True, type='date',string="Du"),
        'recette_date_to':fields.function(lambda *a,**k:{}, method=True, type='date',string="Au"),
        'user_id':fields.many2one('res.users','Utilisateur'),
        'event_id':fields.many2one('event.event', 'Event', domain="[('is_event','=',True)]"),
        'type_event_id':fields.many2one('event.type', 'Type Evenement'),
        'verse':fields.boolean('Est versé'),
  }
recette()

