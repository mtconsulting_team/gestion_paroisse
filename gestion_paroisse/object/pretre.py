# -*- coding: utf-8 -*-
from openerp.osv import osv, fields

class paroisse_pretre(osv.Model):
    _name = 'paroisse.pretre'
    _description = 'Pretre'
    
    _columns = {
            'name' :fields.char('Nom et Prénom du prêtre',required=True),
            'date_naissance': fields.date('Date',required=True), 
            'civilite_pretre':fields.selection([
                ('abbe','Abbé'),
                ('pere','Père'),
                ('eveque','Eveque'),
                ('archeveque','Archeveque'),
                ('cardinal','Cardinal'),
                   ],'Le prêtre'),
            
           
} 
    
    
    
paroisse_pretre()