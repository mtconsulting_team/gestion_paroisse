# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
import pdb

class paroisse_mariage(osv.osv):
    _name = 'paroisse.mariage'
    _description = 'Mariage'
    
    def _get_test_f_deces(self, cr, uid, ids,field, arg, context=None):
        res = {} 
        records = self.browse(cr,uid,ids,context)       
        for record in records:          
            res[record['id']] = record.marie_id.date_deces  and True or False
        return res
    
    def _get_test_m_deces(self, cr, uid, ids,field, arg, context=None):
        res = {} 
        records = self.browse(cr,uid,ids,context)       
        for record in records:          
            res[record['id']] = record.mari_id.date_deces  and True or False
        return res
    
    
    def write(self, cr, uid, ids, vals, context=None):
        v_name = None
        v_firstname = None

        if vals.get('prenom_temoin1'):
            v_firstname = vals['prenom_temoin1'].strip()
            vals['prenom_temoin1'] = v_firstname.title()
        if vals.get('nom_temoin1'):
            v_firstname = vals['nom_temoin1'].strip()
            vals['nom_temoin1'] = v_firstname.title()
            
        if vals.get('prenom_temoin2'):
            v_firstname = vals['prenom_temoin2'].strip()
            vals['prenom_temoin2'] = v_firstname.title()
        if vals.get('nom_temoin2'):
            v_firstname = vals['nom_temoin2'].strip()
            vals['nom_temoin2'] = v_firstname.title()
        
        if vals.get('prenom_temoin3'):
           v_firstname = vals['prenom_temoin3'].strip()
           vals['prenom_temoin3'] = v_firstname.title() 
        if vals.get('nom_temoin3'):
           v_firstname = vals['nom_temoin3'].strip()
           vals['nom_temoin3'] = v_firstname.title()  
        
        if vals.get('prenom_temoin4'):
            v_firstname = vals['prenom_temoin4'].strip()
            vals['prenom_temoin4'] = v_firstname.title() 
        if vals.get('nom_temoin4'):
            v_firstname = vals['nom_temoin4'].strip()
            vals['nom_temoin4'] = v_firstname.title()
            
    ##    if vals.get('parent_comp_id'):
     ##       v_firstname = vals['parent_comp_id'].strip()
      ##      vals['parent_comp_id'] = v_firstname.title()
         
        if vals.get('pretre_id'):
            vals['pretre_id'] = vals['pretre_id'].title()        
         
        if vals.get('marie_id',False):
            cr.execute("update res_partner set is_maried ='t' and civ_religieuse ='mariage' where id =%s"% vals.get('marie_id'))
        if vals.get('mari_id',False):
            cr.execute("update res_partner set is_maried ='t' and civ_religieuse ='mariage' where id =%s"% vals.get('mari_id'))
        
        result = super(paroisse_mariage,self).write(cr, uid, ids, vals, context=context)
        return result

    def create(self, cr, uid, vals, context=None):
        v_name = None
        v_firstname = None
        
        if vals.get('prenom_temoin1'):
            v_firstname = vals['prenom_temoin1'].strip()
            vals['prenom_temoin1'] = v_firstname.title()
        if vals.get('nom_temoin1'):
            v_firstname = vals['nom_temoin1'].strip()
            vals['nom_temoin1'] = v_firstname.title()
            
        if vals.get('prenom_temoin2'):
            v_firstname = vals['prenom_temoin2'].strip()
            vals['prenom_temoin2'] = v_firstname.title()
        if vals.get('nom_temoin2'):
            v_firstname = vals['nom_temoin2'].strip()
            vals['nom_temoin2'] = v_firstname.title()
        
        if vals.get('prenom_temoin3'):
           v_firstname = vals['prenom_temoin3'].strip()
           vals['prenom_temoin3'] = v_firstname.title() 
        if vals.get('nom_temoin3'):
           v_firstname = vals['nom_temoin3'].strip()
           vals['nom_temoin3'] = v_firstname.title()  
        
        if vals.get('prenom_temoin4'):
            v_firstname = vals['prenom_temoin4'].strip()
            vals['prenom_temoin4'] = v_firstname.title() 
        if vals.get('nom_temoin4'):
            v_firstname = vals['nom_temoin4'].strip()
            vals['nom_temoin4'] = v_firstname.title()  
            
            
##        if vals.get('parent_comp_id'):
  ##          vals['parent_comp_id'] = vals['parent_comp_id'].title()
            
        if vals.get('pretre_id'):
            vals['pretre_id'] = vals['pretre_id'].title()


        result = super(paroisse_mariage,self).create(cr, uid, vals, context=context)
        return result

    # def fonction_transfert(self, cr, uid, ids, name, args, context=None):
    #     result = {}
    #     for obj in self.browse(cr, uid, ids, context):
    #         if obj.partner_id:
    #             result[obj.id] = obj.partner_id.ref or False
    #     return result

    def fonction_transfert(self, cr,uid, ids,name,arg, context=None):
         res = {}
         zone_obj = self.pool.get('paroisse.je_soussigne')
         zones = zone_obj.browse(cr, uid, ids, context=context)
         for zone in zones:
            res[zone.id] = zone.nom_paroisse
             #    return {'value':{'zone_residence': zone.zone_residence.id}}
         # return {'value': res}
         # return {'value':{'name':zone.nom_paroisse2}}
         return res

# This function automatically sets the currency to EUR.
    def _get_default_diocese_id(self, cr, uid, context=None):
        res = self.pool.get('paroisse.je_soussigne').search(cr, uid, [('name','=','Archevecher Dakar')], context=context)
        return res and res[0] or False

    def unlink(self,cr,uid,id,context):  
       # 
        mariage =self.browse(cr,uid,id,context)
        cr.execute("update res_partner set is_maried ='f' and civ_religieuse ='prof_foi' where id in "+str((mariage.mari_id.id,mariage.marie_id.id)))
        return super(paroisse_mariage,self).unlink(cr,uid,id,context)
                       
    
    _order = 'date_mariage desc'
    _columns = {
            'name' :fields.char('Numéro mariage'),
            'date_mariage': fields.date('Date mariage'),
            'mari_id':fields.many2one('res.partner','Marié', required=True, domain="['&',('is_paroissien','=',True),('sexe','=','m'),('is_maried','=',False),('civ_religieuse','!=','decede')]"),
            'marie_id':fields.many2one('res.partner','Mariée', required=True, domain="['&',('is_paroissien','=',True),('sexe','=','f'),('is_maried','=',False),('civ_religieuse','!=','decede')]"),
            'date_bapteme_mari':fields.date('Date baptème', ),
            'date_naissance':fields.date('Date naissance', ),
            'nom_marie':fields.char('Nom mari', ),
            'pere_mari':fields.char('Nom du père', ),
            'mere_mari':fields.char('Nom de la mère', ),
            'pretre_id':fields.char('Célébré par'),
            'temoin1' :fields.char('Temoin 1',required=False),
            'temoin2' :fields.char('Temoin 2',required=False),
            'temoin3' :fields.char('Temoin 3',required=False),
            'temoin4' :fields.char('Temoin 4',required=False),
            
            'prenom_temoin1' :fields.char('Prenom Témoin 1'),
            'nom_temoin1' :fields.char('Nom Temoins 1'),
            'prenom_temoin2' :fields.char('Prénom Temoin 2'),
            'nom_temoin2' :fields.char('Nom Temoin 2'),
            'prenom_temoin3' :fields.char('Prénom Temoin 3',required=False),
            'nom_temoin3' :fields.char('Nom Temoin 3',required=False),
            'prenom_temoin4' :fields.char('Prénom Temoin 4',required=False),
            'nom_temoin4' :fields.char('Nom Temoin 4',required=False),
            'lieu_mariage': fields.char('Lieu mariage'),

            'actif':fields.boolean('Active'),
            'pretre_id':fields.char('Célébré par'),
            'comp_id':fields.many2one('res.company','Eglise'),
            'comp2_id':fields.many2one('res.company','Eglise'),

            'parent_comp_id':fields.many2one('res.company','Diocèse'),
            'date_notification': fields.date('Date notification'),
            'nbr_publication': fields.integer(default=1, string='Nombre de publication des bans'),
            'date_publication_ban': fields.char('Date de publication'),
            'dispense_ids': fields.many2many('paroisse.dispense_accordee',
                    'rel_mariage_dispense', 'dispenses_id', 'obtenues_id', 'Dipenses obtenues'),

            'is_f_decede': fields.function(_get_test_f_deces, method=True,
            type='boolean', string='Décès',),
            'is_m_decede': fields.function(_get_test_m_deces, method=True,
                 type='boolean', string='Décès',),

# Moi
    #  Zones créées pour les variables de l'édition'
    # 'diocese_id permet d'activer la connection sur le model paroisse.je_soussigne
    #         'diocese_id': fields.many2one('paroisse.je_soussigne', default='Archidiocèse de Dakar', string='Diocèse du saisissant'),
            'diocese_id': fields.many2one('paroisse.je_soussigne','Diocèse du saisissant'),

    # Creation des champs suivants juste nécessaires pour les vues des fichiers xml
    #         'zone_je_soussigne': </strong>fields.char('Je soussigne'),
        'nom_paroisse': fields.char('Nom paroisse'),
        'adresse_paroisse': fields.char('Adresse paroisse'),
        'boite_postale': fields.char('Boite postale paroisse'),
        'telephone_paroisse': fields.char('Téléphone paroisse'),
        'email_paroisse': fields.char('Email paroisse'),
        'lieu_presbytere': fields.char('Lieu du presbytère'),

 # ------------------------------------Rapport QWEB--------------------------------------------------------------
   # Zones pour Qweb Version-3 ----------------------------------------------------
        'name': fields.char('Numéro mariage'),
   #      'name': fields.many2one('res.company', 'Paroisse'),
        'paroisse_connectant_id': fields.many2one('paroisse.je_soussigne3', 'Paroisse du user'),
        'paroisse_user_id': fields.many2one('res.company', 'Paroisse du user_id'),

        # 'paroisse_connectant_id': fields.char('Paroisse', Default='Ste-Therese'),
        'name3_id': fields.many2one('res.company', 'Paroisse du saisissant'),
        'zone_je_soussigne3': fields.char('Je soussigne3'),
        'diocese_paroisse3': fields.char('Diocese du saisissant3'),
        'adresse_paroisse3': fields.char('Adresse de la paroisse3'),
        'boite_postale3': fields.char('Boite postale3'),
        'telephone_paroisse3': fields.char('Téléphone paroisse3'),
        'email_paroisse3': fields.char('Email paroisse3'),
        'lieu_presbytere3': fields.char('Lieu du presbytere3'),

      }

    # def onchange_mari_id(self,cr,uid,ids,mari_id,paroisse_user_id,paroisse_connectant_id,context=None):
    #     res = {}
        # res['paroisse_connectant_id'] = self.browse(cr, uid, ids[0], context=context).paroisse_user_id.id
        # return self.write(cr, uid, ids, {'paroisse_user_id': paroisse_connectant_id.id}, context=context)


        # res['paroisse_connectant_id'] =paroisse_user_id.id
        # res['recherche'] = paroisse_connectant_id
        # coordonnees = self.pool.get('paroisse.je_soussigne3').browse(cr,uid,paroisse_connectant_id,context=context)
        # pdb.set_trace()

        # res['zone_je_soussigne3']= coordonnees.zone_je_soussigne3,
        # res['diocese_paroisse3'] = coordonnees.diocese_paroisse3
        # res['adresse_paroisse3'] = coordonnees.adresse_paroisse3,
        # res['boite_postale3'] = coordonnees.boite_postale3,
        # res['telephone_paroisse3'] = coordonnees.telephone_paroisse3,
        # res['email_paroisse3'] = coordonnees.email_paroisse3,
        # res['lieu_presbytere3'] = coordonnees.lieu_presbytere3,
        #
        # return {'value': res}

    #
    # def onchange_marie_id(self,cr,uid,ids,marie_id,paroisse_user_id,paroisse_connectant_id,context=None):
    #     res = {}
    #     rec = {}
        # res['paroisse_connectant_id'] = paroisse_user_id.id

        # obj = self.pool.get('paroisse.je_soussigne3')
        # coordonnees = obj.browse(cr,uid,paroisse_connectant_id,context=context)
        # pdb.set_trace()

        # return {'value':
        #             {
        #                 'zone_je_soussigne3': coordonnees.zone_je_soussigne3,
        #                 'diocese_paroisse3': coordonnees.diocese_paroisse3,
        #                 'adresse_paroisse3': coordonnees.adresse_paroisse3,
        #                 'boite_postale3': coordonnees.boite_postale3,
        #                 'telephone_paroisse3': coordonnees.telephone_paroisse3,
        #                 'email_paroisse3': coordonnees.email_paroisse3,
        #                 'lieu_presbytere3': coordonnees.lieu_presbytere3,
        #             }
        #         }

    def create_mariage(self, cr, uid, ids, context={}):
         values ={}
         values['civ_religieuse']='mariage'
         values['is_maried']=True
        # super(paroisse_mariage, self).create(cr,uid,id)
         wizard = self.browse(cr,uid,ids[0],context)
         partner_obj = self.pool.get("res.partner")

         mariage_valuse = {
            'numero_mariage':  wizard.name,
            'date_mariage': wizard.date_mariage,
            'mari_id':wizard.mari_id.id,
            'marie_id':wizard.marie_id.id,
            'date_bapteme_mari':wizard.date_bapteme_mari,
            #'date_naissance':wizard.date_naissance  ,
            'nom_marie':wizard.nom_marie,
            'pere_mari':wizard.pere_mari,
            'mere_mari':wizard.mere_mari,
            'pretre_id':wizard.pretre_id,
            'prenom_temoin1':wizard.prenom_temoin1,
            'nom_temoin1':wizard.nom_temoin1,
            'prenom_temoin2':wizard.prenom_temoin2,
            'nom_temoin2':wizard.nom_temoin2,
            'prenom_temoin3':wizard.prenom_temoin3,
            'nom_temoin3':wizard.nom_temoin3,
            'prenom_temoin4':wizard.prenom_temoin4,
            'nom_temoin4':wizard.nom_temoin4,

        # Infos pour Qweb
        #      'name3_id': wizard.name3_id,
        #      'zone_je_soussigne3': wizard.zone_je_soussigne3,
        #      'diocese_paroisse3': wizard.diocese_paroisse3,
        #      'adresse_paroisse3': wizard.adresse_paroisse3,
        #      'boite_postale3': wizard.boite_postale3,
        #      'telephone_paroisse3': wizard.telephone_paroisse3,
        #      'email_paroisse3': wizard.email_paroisse3,
        #      'lieu_presbytere3': wizard.lieu_presbytere3,


            'actif':wizard.actif,
            'pretre_id':wizard.pretre_id  ,
            'comp_id':wizard.comp_id.id  ,
            # 'parent_comp_id':wizard.parent_comp_id.id,
            'is_maried':True,
            'civ_religieuse':'mariage',
# Pour repositionner en invisible les boutons Mariage
            'accord_obtenu_f': False,
            'accord_obtenu_m': False


            }

         partner_obj.write(cr, uid, wizard.mari_id.id, mariage_valuse, context={})
         partner_obj.write(cr, uid, wizard.marie_id.id, mariage_valuse, context={})

         self.pool.get("res.partner").write(cr,uid,wizard.mari_id.id,values)  
         self.pool.get("res.partner").write(cr,uid,wizard.marie_id.id,values)

         return True
    
    _defaults = {  
        'actif': True,
        # 'paroisse_connectant_id': 'Ste-Therese',
  #      'comp_id': lambda self, cr, uid, ctx: self.pool.get('res.company')._company_default_get(cr, uid, 'res.partner', context=ctx),
         'diocese_id': _get_default_diocese_id,
         # 'name3_id': lambda self, cr, uid, ctx: self.pool.get('res.company')._company_default_get(cr, uid, 'res.partner', context=ctx),

         'paroisse_user_id': lambda self, cr, uid, ctx: self.pool.get('res.company')._company_default_get(cr, uid, 'res.partner', context=ctx),



                  }

paroisse_mariage()


class paroisse_date_pub_bans(osv.osv):
    _name = "paroisse.date_pub_bans"

    _columns = {
        'name': fields.date('Dates publications'),
    }

class paroisse_dispense_accordee(osv.osv):
    _name = "paroisse.dispense_accordee"

    _columns = {
        'name': fields.char('Dispenses obtenues'),
    }

class paroisse_res_company(osv.Model):

    _inherit = 'res.company'

    _columns = {
        'lieu_presbytere': fields.char('Lieu du presbytère'),
        'signature_cure': fields.text('Je_soussigné du curé'),
        'code_paroisse': fields.char('Code paroisse'),
    }
