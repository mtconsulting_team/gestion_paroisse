# -*- coding: utf-8 -*-
#----------------------------------------------------------------------------
#
#    Copyright (C) 2014  .
#    TeamIT
#
#----------------------------------------------------------------------------
from openerp.osv import osv
from openerp.report import report_sxw
#-------------------------------------------------------------------------------
# journal_salary_report
#-------------------------------------------------------------------------------
class event_list(report_sxw.rml_parse):

    def __init__(self, cr, uid, name, context):
        super(event_list, self).__init__(cr, uid, name, context=context)
        
        self.localcontext.update({
                                  
        'get_all_line' : self._get_all_line,
        'get_custom_date' : self._get_custom_date,
         
        })
    def _get_custom_date(self):
               
        event_obj1 = self.pool.get('event.list')
        custom_date = event_obj1.browse(self.cr, self.uid, self.ids[0])          
        return custom_date

    def _get_all_line(self):
        event_obj = self.pool.get('event.event')        
        event_obj1 = self.pool.get('event.list')
        
        data = event_obj1.browse(self.cr, self.uid, self.ids[0])  
        search = event_obj.search(self.cr, self.uid, ['&', ('date_begin', '>=', data.date_begin), ('date_begin', '<=', data.date_end)])
        
        all_line = event_obj.browse(self.cr, self.uid, search)
        
        return all_line
    
class report_event_list(osv.AbstractModel):
    _name = 'report.gestion_paroisse.event_list'
    _inherit = 'report.abstract_report'
    _template = 'gestion_paroisse.event_list'
    _wrapped_report_class = event_list
    