# -*- coding: utf-8 -*-

from openerp.osv import fields, osv
import time
from datetime import datetime

AVAILABLE_STATES = [
    ('draft', 'New'),
    ('cancel', 'Cancelled'),
    ('open', 'In Progress'),
    ('pending', 'Pending'),
    ('done', 'Réalisé')
]

DT_FMT = '%Y-%m-%d %H:%M:%S'
class ActionClass(osv.osv):
    _inherit = "crm.helpdesk"
    
    def default_get(self, cr, uid, fields, context=None):
        res = super(ActionClass,self).default_get(cr, uid, fields, context)
        partner_id =context.get('partner_id',[])   
        interlocutor_id =context.get('interlocutor_id',[])           
        if partner_id:            
            res.update({'partner_id': partner_id})  
            
        if interlocutor_id:
                      res.update({'interlocutor_id': interlocutor_id})  
        return res

    def _date_realised(self, cursor, user, ids, name, args, context=None):
        if not ids:
            return {}
        res = {}
        for action in self.browse(cursor, user, ids, context=context):
            if action.state == 'done':
                res[action.id] = datetime.now().strftime(DT_FMT)
        return res

    def _action_realised(self, cursor, user, ids,name, args, context=None):
        if not ids:
            return {}
        res = {}
        for action in self.browse(cursor, user, ids, context=context):
            if action.state == 'done':
                res[action.id] = True
        return res

    def _get_ref_partner(self, cr, uid, ids, name, args, context=None):
        result = {}
        for obj in self.browse(cr, uid, ids, context):
            if obj.partner_id:
                result[obj.id] = obj.partner_id.ref or False
        return result

    def _client_internal_name(self, cr, uid, ids, name, args, context=None):
        result = {}
        for obj in self.browse(cr, uid, ids, context):
            if obj.type == 'intern':
                result[obj.id] = 'Interne'
            elif obj.type == 'client':
                result[obj.id] = obj.partner_id.name
        return result

    _columns = {
        'type': fields.selection([('client', 'Paroissien'), ('intern','Internal'),], 'Type'),
        'commentaire_cloture': fields.text('Action'),
        'emis_id': fields.many2one('res.users', 'Responsible'),
        'interlocutor_id': fields.many2one('res.partner', string='Interlocutor'),
        'function': fields.related ( 'interlocutor_id', 'fonction_id', type='many2one', string='Function' ),
        'tel': fields.related ( 'interlocutor_id', 'phone', type='char', string='Phone' ),
        'notice': fields.date('Notice'),
        'action_realised': fields.function(_action_realised, type='boolean', store=True, string='Action Realised'),
        'realised_date': fields.function(_date_realised, type='datetime', store=True, string='Realised Date'),
        'ref': fields.function(_get_ref_partner, type='char', store=True, string='Ref Client'),
        'action_date_from':fields.function(lambda *a,**k:{}, method=True, type='date',string="Order date from"),
        'action_date_to':fields.function(lambda *a,**k:{}, method=True, type='date',string="Order date to"),
        'state': fields.selection(AVAILABLE_STATES, 'Status', size=16, readonly=True,
                                  help='The status is set to \'Draft\', when a case is created.\
                                  \nIf the case is in progress the status is set to \'Open\'.\
                                  \nWhen the case is over, the status is set to \'Done\'.\
                                  \nIf the case needs to be reviewed then the status is set to \'Pending\'.'),
        'internal_client': fields.function(_client_internal_name, type='char', store=True, string='Client/Internal'),
    }

    _defaults = {
        'name': 'Draft Action/',
        'type' : 'client',
    }

    def onchange_partner_id(self, cr, uid, ids, part, email=False, context = None):
        result = super(ActionClass, self).onchange_partner_id(cr, uid, ids, part, email)
        if part:
            obj = self.pool.get('res.partner').browse(cr, uid, part, context = context)
            result['value']['ref'] = obj.ref
        return result

    def create(self, cr, uid, vals, context=None):
        vals['name'] = self.pool.get('ir.sequence').get(cr, uid, 'crm.helpdesk') or '/'
        return super(ActionClass, self).create(cr, uid, vals, context=context)

    def onchange_interlocutor_id(self, cr, uid, id, partner_id, context=None):
        result = {}
        if partner_id:
            part = self.pool.get('res.partner').browse(cr, uid, partner_id, context=context)
            result['tel'] = part.phone or False
            result['function'] = part.fonction_id.id or False
        return {'value': result}