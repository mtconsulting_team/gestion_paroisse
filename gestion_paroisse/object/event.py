# -*- coding: utf-8 -*-
from datetime import datetime, timedelta
from openerp.osv import fields, osv
from openerp.tools.translate import _
from openerp import netsvc
from openerp import tools, SUPERUSER_ID
from dateutil import rrule
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
import time
import datetime


import pytz
import re
import time
import openerp
import openerp.service.report
import uuid
import collections
import babel.dates
from werkzeug.exceptions import BadRequest
from datetime import datetime, timedelta
from dateutil import parser
from dateutil import rrule
from dateutil.relativedelta import relativedelta
from openerp import api
from openerp import tools, SUPERUSER_ID
from openerp.osv import fields, osv
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from openerp.tools.translate import _
from openerp.http import request
from operator import itemgetter

class event_motion(osv.osv):
    _name = 'event.motion'
    _description =' Motion'
    

class event_type(osv.osv):
    _inherit = 'event.type'
    
    def _get_empty_rrule_data(self):
        return {
            'byday': False,
            'recurrency': False,
            'final_date': False,
            'rrule_type': False,
            'month_by': False,
            'interval': 0,
            'count': False,
            'end_type': False,
            'mo': False,
            'tu': False,
            'we': False,
            'th': False,
            'fr': False,
            'sa': False,
            'su': False,
            'day': False,
            'week_list': False
        }
        
    def _parse_rrule(self, rule, data, date_start):
        day_list = ['mo', 'tu', 'we', 'th', 'fr', 'sa', 'su']
        rrule_type = ['yearly', 'monthly', 'weekly', 'daily']
        r = rrule.rrulestr(rule, dtstart=datetime.strptime(date_start, DEFAULT_SERVER_DATETIME_FORMAT))

        if r._freq > 0 and r._freq < 4:
            data['rrule_type'] = rrule_type[r._freq]
        data['count'] = r._count
        data['interval'] = r._interval
        data['final_date'] = r._until and r._until.strftime(DEFAULT_SERVER_DATETIME_FORMAT)
        #repeat weekly
        if r._byweekday:
            for i in xrange(0, 7):
                if i in r._byweekday:
                    data[day_list[i]] = True
            data['rrule_type'] = 'weekly'
        #repeat monthly by nweekday ((weekday, weeknumber), )
        if r._bynweekday:
            data['week_list'] = day_list[r._bynweekday[0][0]].upper()
            data['byday'] = str(r._bynweekday[0][1])
            data['month_by'] = 'day'
            data['rrule_type'] = 'monthly'

        if r._bymonthday:
            data['day'] = r._bymonthday[0]
            data['month_by'] = 'date'
            data['rrule_type'] = 'monthly'

        #repeat yearly but for openerp it's monthly, take same information as monthly but interval is 12 times
        if r._bymonth:
            data['interval'] = data['interval'] * 12

        #FIXEME handle forever case
        #end of recurrence
        #in case of repeat for ever that we do not support right now
        if not (data.get('count') or data.get('final_date')):
            data['count'] = 100
        if data.get('count'):
            data['end_type'] = 'count'
        else:
            data['end_type'] = 'end_date'
        return data
    
    def _set_rulestring(self, cr, uid, ids, field_name, field_value, args, context=None):
        if not isinstance(ids, list):
            ids = [ids]
        data = self._get_empty_rrule_data()
        if field_value:
            data['recurrency'] = True
            for event in self.browse(cr, uid, ids, context=context):
                rdate = event.date_start,
                print str(rdate)
                update_data = self._parse_rrule(field_value, dict(data),rdate)
                data.update(update_data)
                self.write(cr, uid, ids, data, context=context)
        return True
    
    def compute_rule_string(self, data):
        """
        Compute rule string according to value type RECUR of iCalendar from the values given.
        @param self: the object pointer
        @param data: dictionary of freq and interval value
        @return: string containing recurring rule (empty if no rule)
        """
        if data['interval'] and data['interval'] < 0:
            raise osv.except_osv(_('warning!'), _('interval cannot be negative.'))
        if data['count'] and data['count'] <= 0:
            raise osv.except_osv(_('warning!'), _('count cannot be negative or 0.'))
    
    def _get_rulestring(self, cr, uid, ids, name, arg, context=None):
        """
        Gets Recurrence rule string according to value type RECUR of iCalendar from the values given.
        @return: dictionary of rrule value.
        """
        result = {}
        if not isinstance(ids, list):
            ids = [ids]

        #read these fields as SUPERUSER because if the record is private a normal search could raise an error
        events = self.read(cr, SUPERUSER_ID, ids,
                           ['id', 'byday', 'recurrency', 'final_date', 'rrule_type', 'month_by',
                            'interval', 'count', 'end_type', 'mo', 'tu', 'we', 'th', 'fr', 'sa',
                            'su', 'day', 'week_list'], context=context)
        for event in events:
            if event['recurrency']:
                result[event['id']] = self.compute_rule_string(event)
            else:
                result[event['id']] = ''

        return result
    
    def _find_my_attendee(self, cr, uid, meeting_ids, context=None):
        """
            Return the first attendee where the user connected has been invited from all the meeting_ids in parameters
        """
        user = self.pool['res.users'].browse(cr, uid, uid, context=context)
        for meeting_id in meeting_ids:
            for attendee in self.browse(cr, uid, meeting_id, context).attendee_ids:
                if user.partner_id.id == attendee.partner_id.id:
                    return attendee
        return False
    
    
    def _compute(self, cr, uid, ids, fields, arg, context=None):
        res = {}
        if not isinstance(fields, list):
            fields = [fields]
        for meeting in self.browse(cr, uid, ids, context=context):
            meeting_data = {}
            res[meeting.id] = meeting_data
#             attendee = self._find_my_attendee(cr, uid, [meeting.id], context)
            for field in fields:
                
                if field == "display_start":
                    meeting_data[field] = meeting.date_start
                elif field == 'start':
                    meeting_data[field] = meeting.date_start 
                elif field == 'stop':
                    meeting_data[field] = meeting.date_start 
        return res
    
    def onchange_recurent(self, cr, uid, ids, recurrency, context=None):
        if recurrency:
#             rec = self.pool.get("event.type").browse(cr,uid,ids,context=context)
#             if rec :
              #  res['value']['is_nb_participant'] =rec.is_nb_participant
            return {'value': {'is_nb_participant': True, }}
    
    
    
    _columns = {
    'is_event_payant':fields.boolean('Evènement payant', required=False), 
    'prix':fields.float('Prix', digits=(16,0)),    
    'is_nb_participant': fields.boolean('On va gérer les nombres de participants ?'),
    'categ_type_ids':fields.many2one('categ.type','Categorie type'), 
    'date_start':  fields.datetime('Date Start'),
    
    # RECURRENCE FIELD
#         'rrule': fields.function(_get_rulestring, type='char', fnct_inv=_set_rulestring, store=True, string='Recurrent Rule'),
        'rest_count': fields.integer('Rest Repeat', help="Repeated x times"),
        'rrule_type': fields.selection([('daily', 'Day(s)'), ('weekly', 'Week(s)'), ('monthly', 'Month(s)'), ('yearly', 'Year(s)')], 'Recurrency', help="Let the event automatically repeat at that interval"),
        'recurrency': fields.boolean('Recurrent', help="Recurrent Meeting"),
        'recurrent_id': fields.integer('Recurrent ID'),
        'recurrent_id_date': fields.datetime('Recurrent ID date'),
        'end_type': fields.selection([('count', 'Number of repetitions'), ('end_date', 'End date')], 'Recurrence Termination'),
        'interval': fields.integer('Repeat Every', help="Repeat every (Days/Week/Month/Year)"),
        'count': fields.integer('Repeat', help="Repeat x times"),
        'lu': fields.boolean('Lun'),
        'ma': fields.boolean('Mar'),
        'me': fields.boolean('Mer'),
        'je': fields.boolean('Jeu'),
        've': fields.boolean('Ven'),
        'sa': fields.boolean('Sam'),
        'di': fields.boolean('Dim'),
        'month_by': fields.selection([('date', 'Date of month'), ('day', 'Day of month')], 'Option', oldname='select1'),
        'day': fields.integer('Date of month'),
        'week_list': fields.selection([('MO', 'Monday'), ('TU', 'Tuesday'), ('WE', 'Wednesday'), ('TH', 'Thursday'), ('FR', 'Friday'), ('SA', 'Saturday'), ('SU', 'Sunday')], 'Weekday'),
        'byday': fields.selection([('1', 'First'), ('2', 'Second'), ('3', 'Third'), ('4', 'Fourth'), ('5', 'Fifth'), ('-1', 'Last')], 'By day'),
        'final_date': fields.date('Repeat Until'),  # The last event of a recurrence
        'start': fields.function(_compute, string='Calculated start', type="datetime", multi='attendee', store=True),
    
        
    }
    
    
    _defaults = {
                  'date_start':  lambda *a: fields.datetime.now(),
                  }
    
   


event_type()

class event(osv.osv):
    _inherit = 'event.event'
    
    _order ="date_begin,name"
    
    def name_get(self, cr, uid, ids, context=None):
        if not ids:
            return []

        if isinstance(ids, (long, int)):
            ids = [ids]

        res = []
        for record in self.browse(cr, uid, ids, context=context):
            date = record.date_begin.split(" ")[0]
            date_end = record.date_end.split(" ")[0]
            if date != date_end:
                date += ' - ' + date_end
            display_name = record.name +" "+record.type.name+" "+ ' (' + date + ')'
            res.append((record['id'], display_name))
        return res

    
    def onchange_event_type(self, cr, uid, ids, type_event, context=None):
#         res = super(event, self).onchange_event_type( cr, uid, ids, type_event,context=None)
        res = {}
        if type_event:
            rec = self.pool.get("event.type").browse(cr,uid,type_event,context=context)
            if rec :
              #  res['value']['is_nb_participant'] =rec.is_nb_participant
              return {'value': {'is_nb_participant': rec.is_nb_participant, }}
          
    
    
    
#     def confirm_event(self, cr, uid, ids, context=None):
#         register_pool = self.pool.get('event.registration')
# #         event = self.event
#         event = self.browse(cr,uid,ids[0])
#         if event.email_confirmation_id:
#         #send reminder that will confirm the event for all the people that were already confirmed
#             reg_ids = register_pool.search(cr, uid, [
#                                ('event_id', '=', self.event.id),
#                                ('state', 'not in', ['draft', 'cancel'])], context=context)
#             register_pool.mail_user_confirm(cr, uid, reg_ids)           
#             
#                       
# 
#         return self.write(cr, uid, ids, {'state': 'confirm'}, context=context) 
    
    
    def confirm_event(self, cr, uid, ids, context=None):
        register_pool = self.pool.get('event.registration')
#         event = self.event
        event = self.browse(cr,uid,ids[0])
        if event.email_confirmation_id:
        #send reminder that will confirm the event for all the people that were already confirmed
            reg_ids = register_pool.search(cr, uid, [
                               ('event_id', '=', self.event.id),
                               ('state', 'not in', ['draft', 'cancel'])], context=context)
            register_pool.mail_user_confirm(cr, uid, reg_ids)           
              
        if event.type.is_event_payant:
            if event.type.is_nb_participant and len(event.registration_ids)>0:
                for reg in event.registration_ids:
                    o = self.generate_facture(cr,uid,event,reg.partner_id,context)
            if event.paroissien_id:
                    o = self.generate_facture(cr,uid,event,event.paroissien_id,context)
            return self.write(cr, uid, ids, {'state': 'confirm','invoice_id' : o}, context=context)
        else: self.write(cr, uid, ids, {'state': 'confirm'}, context=context)
    
    def generate_facture(self,cr,uid,event,paroissien_id,context):
            invoice_obj = self.pool.get('account.invoice')
            invoice_id = False
            if paroissien_id:     
                invoice_ids =invoice_obj.search(cr,uid,[('partner_id','=',paroissien_id.id),('state','=','open')])
                if invoice_ids and len(invoice_ids)>0:
                    invoice_id= invoice_ids[0]
                    invoice_line_obj = self.pool.get('account.invoice.line')                              
                    quantity = 1
                    line_value =  {                           
                        'name':event.name
                        }
            #Recuperer les valeurs des champs calculer en fonction des produits, clients ...
                    line_dict = invoice_line_obj.product_id_change(cr, uid, {},
                            False, False, quantity, '', 'out_invoice', paroissien_id.id, False, price_unit=event.type.prix, context=context)
                
                    line_value.update(line_dict['value'])
                    line_value['price_unit'] = event.type.prix                      
                    line_value['invoice_id'] = invoice_id
                    #creation  de la ligne
                    invoice_obj.write(cr, uid, invoice_id, {'invoice_line': [(0, 0, line_value)]}, context=context)
                    
                    #suppression des account_move_line
                    invoice_object = invoice_obj.browse(cr, uid, invoice_id)
                    invoice_object.write({'origin': invoice_object.origin + ', ' + event.name})
                    mo_id = invoice_object.move_id.id
                    invoice_object.write({'move_id': False})
                    cr.execute('''DELETE FROM account_move_line l\
                                    WHERE l.move_id = %s''',(mo_id,))
                    
                    cr.execute('''DELETE FROM account_move l\
                                    WHERE l.id = %s''',(mo_id,))
                    invoice_object.action_move_create()
                    #netsvc.LocalService('workflow').trg_validate(uid, 'account.invoice', invoice_id, 'invoice_open', cr)

                else:
                    account_id = paroissien_id.property_account_receivable and paroissien_id.property_account_receivable.id or False            
                    values = {
                       'partner_id': paroissien_id.id,
                       'account_id': account_id,
                       'payment_term': False,
                       'type_event_id':event.type.id,
                       'origin':event.name,
                     #'credit':partner.payment_amount_overdue,
                       'fiscal_position': False,
                       'event_id':event.id
                           }
                    invoice_id = invoice_obj.create(cr,uid,values, context) 
                                 
                    invoice_line_obj = self.pool.get('account.invoice.line')                              
                    quantity = 1
                    line_value =  {                           
                        'name':event.name
                        }
            #Recuperer les valeurs des champs calculer en fonction des produits, clients ...
                    line_dict = invoice_line_obj.product_id_change(cr, uid, {},
                            False, False, quantity, '', 'out_invoice', paroissien_id.id, False, price_unit=event.type.prix, context=context)
                
                    line_value.update(line_dict['value'])
                    line_value['price_unit'] = event.type.prix                      
                    line_value['invoice_id'] = invoice_id
             #creation  de la ligne
                    invoice_line_id = invoice_line_obj.create(cr, uid, line_value, context=context)
                    invoice_obj.write(cr, uid, invoice_id, {'invoice_line': [(6, 0, [invoice_line_id])]}, context=context)
                    netsvc.LocalService('workflow').trg_validate(uid, 'account.invoice', invoice_id, 'invoice_open', cr)
           
                    self.pool.get('res.partner').write(cr, uid, paroissien_id.id, {})
            return invoice_id
    
    _columns = {
    
    'is_nb_participant': fields.boolean('On va gérer les nombres de participants ?'),
    'categ_type_ids':fields.many2one('categ.type','Categorie type'),
    'invoice_id':fields.many2one('account.invoice','Invoice'),
    'paroissien_id':fields.many2one('res.partner','Paroissien'),
    'is_event':fields.boolean('Event'), 
}
    _defaults = {
                  'is_event': True,
                  }
    
    def create_event(self, cr, user, recur_task, context=None):
#             task_obj = self.pool.get('project.task')
            event_obj = self.pool.get('event.event')
#             project_id=self.pool.get('event.event').search(cr, user, [('analytic_account_id','=',recur_task.contrat_id.id)])
#             project=self.pool.get('project.project').browse(cr, user, project_id, context)
#             date = datetime.datetime.now()
#             jours_feries = self.pool.get("jours.feries").search(cr, user, [('jour', '=', date.day),('mois','=', date.month )])
#             if not jours_feries:
#                     date_start= datetime.now()
#             else:
#                 i=1
#                 while (jours_feries):
#                     next_date= datetime.now() + relativedelta(days=+i)
#                     jours_feries = self.pool.get("jours.feries").search(cr, user, [('jour', '=', next_date.day),('mois','=',next_date.month )])
#                     i=i+1
#                 date_start= next_date    
            date_start= datetime.now()
            vals={
                          'name':recur_task.name,
                          'date_end':date_start,
                          'date_begin': date_start,
#                           'paroissien_id':recur_task,R
                          'type':recur_task.id
#                           'project_id':project.id,
#                           'planned_hours':  self._convert_qty_company_hours(cr, user, recur_task, context=context),
#                           'user_id': recur_task.user_id.id,
#                           'date_start': date_start,
                  }
            event_obj.create(cr, user, vals , context=context)
            if (recur_task.end_type=='count'):
                self.pool.get("event.type").write(cr, user, recur_task.id, {'rest_count':recur_task.rest_count+1,}, context=context)
#             netsvc.LocalService('workflow').trg_validate(user, 'project.task', recur_task.id, 'normal', cr)
   
    
    
    def appel_contrast_task(self, cr, user, context={}):
            
            ids = self.pool.get("event.type").search(cr,user, [('recurrency','=',True)])
            
           # r = rrule.rrulestr({'rrule_type':'monthly', 'month_by':'day', 'week_list':'TU'}, dtstart=datetime.now())
           # print(str(r._bynweekday[0][1]))
            for recur_task in self.pool.get("event.type").browse(cr, user, ids, context):
                ################################################################
                ############################DAILY###############################
                ###############################################################
                if(recur_task.rrule_type=='daily'):
                    if(recur_task.end_type=='count' and recur_task.rest_count < recur_task.count):
                        self.create_event( cr, user, recur_task,context=context)
                    elif(recur_task.end_type=='end_date'): 
                        check_out = datetime.strptime(recur_task.final_date, '%Y-%m-%d')
                        if(datetime.now() < datetime.strptime(recur_task.final_date, '%Y-%m-%d')):
                                self.create_event( cr, user, recur_task,context=context)
                ################################################################
                ############################DAILY###############################
                ###############################################################
                elif(recur_task.rrule_type=='weekly'):
                    date = datetime.now()
                    #print(date.strftime('%a'))
                    if(recur_task.end_type=='count' and recur_task.rest_count < recur_task.count):
                        if(recur_task.lu and date.strftime('%a')=='lun.' ):
                            self.create_event( cr, user, recur_task,context=context)
                        if(recur_task.ma and date.strftime('%a')=='mar.' ):
                            self.create_event( cr, user, recur_task,context=context)
                        if(recur_task.me and date.strftime('%a')=='mer.' ):
                            self.create_event( cr, user, recur_task,context=context)
                        if(recur_task.je and date.strftime('%a')=='jeu.' ):
                            self.create_event( cr, user, recur_task,context=context)
                        if(recur_task.ve and date.strftime('%a')=='ven.' ):
                            self.create_event( cr, user, recur_task,context=context)
                        if(recur_task.sa and date.strftime('%a')=='sam.' ):
                            self.create_event( cr, user, recur_task,context=context)
                        if(recur_task.di and date.strftime('%a')=='dim.' ):
                            self.create_event( cr, user, recur_task,context=context)
                    elif(recur_task.end_type=='end_date'): 
                        check_out = datetime.strptime(recur_task.final_date, '%Y-%m-%d')
                        if(datetime.now() < datetime.strptime(recur_task.final_date, '%Y-%m-%d')):
                                if(recur_task.lu and date.strftime('%a')=='lun.' ):
                                    self.create_event( cr, user, recur_task,context=context)
                                if(recur_task.ma and date.strftime('%a')=='mar.' ):
                                    self.create_event( cr, user, recur_task,context=context)
                                if(recur_task.me and date.strftime('%a')=='mer.' ):
                                    self.create_event( cr, user, recur_task,context=context)
                                if(recur_task.je and date.strftime('%a')=='jeu.' ):
                                    self.create_event( cr, user, recur_task,context=context)
                                if(recur_task.ve and date.strftime('%a')=='ven.' ):
                                    self.create_event( cr, user, recur_task,context=context)
                                if(recur_task.sa and date.strftime('%a')=='sam.' ):
                                    self.create_event( cr, user, recur_task,context=context)
                                if(recur_task.di and date.strftime('%a')=='dim.' ):
                                    self.create_event( cr, user, recur_task,context=context)
                    ################################################################
                    ############################monthy###############################
                    ###############################################################
                elif(recur_task.rrule_type=='monthly'):
                        date = datetime.now()
                       
                        if(recur_task.end_type=='count' and recur_task.rest_count < recur_task.count):
                            if(recur_task.month_by=='date' and recur_task.day==date.day):
                                self.create_event( cr, user, recur_task,context=context)
                            elif(recur_task.month_by=='day'):
                                #print(date.weekday())
                                #print(date.isoweekday())
                                number_week= round(int(date.strftime('%U'))/5 , 0)
                                if(recur_task.byday=='1' and number_week==1):
                                    if(recur_task.week_list=='MO' and date.strftime('%a')=='lun.'):
                                         self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='TU' and date.strftime('%a')=='mer.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='WE' and date.strftime('%a')=='mar.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='TH' and date.strftime('%a')=='jeu.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='FR' and date.strftime('%a')=='ven.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='SA' and date.strftime('%a')=='sam.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='SU' and date.strftime('%a')=='dim.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                if(recur_task.byday=='2' and number_week==2):
                                    if(recur_task.week_list=='MO' and date.strftime('%a')=='lun.'):
                                         self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='TU' and date.strftime('%a')=='mer.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='WE' and date.strftime('%a')=='mar.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='TH' and date.strftime('%a')=='jeu.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='FR' and date.strftime('%a')=='ven.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='SA' and date.strftime('%a')=='sam.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='SU' and date.strftime('%a')=='dim.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                if(recur_task.byday=='3' and number_week==3):
                                    if(recur_task.week_list=='MO' and date.strftime('%a')=='lun.'):
                                         self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='TU' and date.strftime('%a')=='mar.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='WE' and date.strftime('%a')=='mer.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='TH' and date.strftime('%a')=='jeu.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='FR' and date.strftime('%a')=='ven.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='SA' and date.strftime('%a')=='sam.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='SU' and date.strftime('%a')=='dim.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                if(recur_task.byday=='4' and number_week==4):
                                    if(recur_task.week_list=='MO' and date.strftime('%a')=='lun.'):
                                         self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='TU' and date.strftime('%a')=='mer.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='WE' and date.strftime('%a')=='mar.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='TH' and date.strftime('%a')=='jeu.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='FR' and date.strftime('%a')=='ven.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='SA' and date.strftime('%a')=='sam.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='SU' and date.strftime('%a')=='dim.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                if(recur_task.byday=='5' and number_week==5):
                                    if(recur_task.week_list=='MO' and date.strftime('%a')=='lun.'):
                                         self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='TU' and date.strftime('%a')=='mer.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='WE' and date.strftime('%a')=='mar.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='TH' and date.strftime('%a')=='jeu.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='FR' and date.strftime('%a')=='ven.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='SA' and date.strftime('%a')=='sam.'):
                                        self.create_event( cr, user, recur_task,context=context)
                                    elif (recur_task.week_list=='SU' and date.strftime('%a')=='dim.'):
                                        self.create_event( cr, user, recur_task,context=context)
                ###############################################################
                ############################YEARLY#############################
                ###############################################################
                if(recur_task.rrule_type =='yearly'):
                    if(recur_task.end_type=='count' and recur_task.rest_count < recur_task.count):
                        self.create_event( cr, user, recur_task,context=context)
                    elif(recur_task.end_type=='end_date'): 
                        check_out = datetime.strptime(recur_task.final_date, '%Y-%m-%d')
                        if(datetime.now() < datetime.strptime(recur_task.final_date, '%Y-%m-%d')):
                                self.create_event( cr, user, recur_task, context=context)                   
                    
                    
            return {}
  
    
event()

#-------------------------------------------------------------------------------
# categorie_type_event
#-------------------------------------------------------------------------------
class mt_categ_type(osv.osv):
    _name = "categ.type"

    _columns = {
        'name': fields.char("Categorie type", required=True),
    }

# class registration(osv.osv):
#     _inherit = 'event.registration'
#     
#     def _registration_event(self, cr, uid, ids, name, arg, context={}):       
#         result={}
#         for patient_data in self.browse(cr, uid, ids, context=context):
#                 result[patient_data.id] = compute_age_from_dates (patient_data.date_naissance)
#         return result
#     _columns = {
#     
#     'type_evenement_id': fields.function(_registration_event, method=True, type='many2one', string='Type évènement', store=True),
#        
#     }
# 
# 
# event()

class event_inherit_account_voucher(osv.osv):
    _inherit = 'account.voucher' 
    
    
    def button_proforma_voucher(self, cr, uid, id, context):
        context={}
        if context == None:
            context = context
        record =self.browse(cr,uid,id,context)
        recette_obj = self.pool.get("gestion.paroisse.depense.recette")
        values ={
                 'description' : record.event_id.name,
                 'debit' : record.amount,
                 'type' : 'recette',
                 'date_dep' : record.date,
                 'company_id':record.company_id.id,
                 'event_id':record.event_id.id,
                 'type_event_id':record.type_event_id.id,
                  }
        recette_obj.create(cr,uid,values, context) 
        return super(event_inherit_account_voucher, self).button_proforma_voucher(cr, uid, id, context=context)
   
    _columns = {
                'event_id':fields.many2one('event.event', 'Event',domain="[('is_event','=',True)]"),
                'type_event_id':fields.many2one('event.type', 'Type Evenement'), 
                }
    
class event_inherit_account_invoice(osv.osv):
    _inherit = 'account.invoice'
    def invoice_pay_customer(self, cr, uid, ids, context=None):
        if not ids: return []
        dummy, view_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'account_voucher', 'view_vendor_receipt_dialog_form')
        inv = self.browse(cr, uid, ids[0], context=context)
        return {
            'name':_("Pay Invoice"),
            'view_mode': 'form',
            'view_id': view_id,
            'view_type': 'form',
            'res_model': 'account.voucher',
            'type': 'ir.actions.act_window',
            'nodestroy': True,
            'target': 'new',
            'domain': '[]',
            'context': {
                'payment_expected_currency': inv.currency_id.id,
                'default_partner_id': self.pool.get('res.partner')._find_accounting_partner(inv.partner_id).id,
                'default_amount': inv.type in ('out_refund', 'in_refund') and -inv.residual or inv.residual,
                'default_reference': inv.name,
                'default_event_id':inv.event_id.id,
                'default_type_event_id':inv.type_event_id.id,
                'close_after_process': True,
                'invoice_type': inv.type,
                'invoice_id': inv.id,
                'default_type': inv.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
                'type': inv.type in ('out_invoice','out_refund') and 'receipt' or 'payment',
            }}

    _columns = {
                'event_id':fields.many2one('event.event', 'Event', domain="[('is_event','=',True)]"), 
                'partner_id' :fields.many2one('res.partner', string='Paroissien', domain="['&',('is_paroissien','=',True),('civ_religieuse','!=','decede')]"),
                'type_event_id':fields.many2one('event.type', 'Type Evenement'),
                }
    
    
    
    
    
    
    
    
