# -*- coding: utf-8 -*-
from openerp.osv import osv, fields
import sys
from datetime import datetime
import time
from mx import DateTime
from openerp.tools import ustr
sys.setrecursionlimit(1000)


class doyenne(osv.osv):
    _name = "res.doyenne"
    
    def write(self, cr, uid, ids, vals, context=None):
        v_name = None
        v_firstname = None

        if vals.get('name'):
            v_firstname = vals['name'].strip()
            vals['name'] = v_firstname.title()
                            
        result = super(doyenne, self).write(cr, uid, ids, vals, context=context)
        return result

    def create(self, cr, uid, vals, context=None):
        v_name = None
        v_firstname = None

        if vals.get('name'):
            v_firstname = vals['name'].strip()
            vals['name'] = v_firstname.title()
        
        result = super(doyenne, self).create(cr, uid, vals, context=context)
        return result
    
    _columns = {
        'name':fields.char('Doyenné', size=64, required=False, readonly=False),
             }
doyenne()

class communes(osv.osv):
    _name = "res.communes"
    
    def write(self, cr, uid, ids, vals, context=None):
        v_name = None
        v_firstname = None

        if vals.get('name'):
            v_firstname = vals['name'].strip()
            vals['name'] = v_firstname.title()
                            
        result = super(communes, self).write(cr, uid, ids, vals, context=context)
        return result

    def create(self, cr, uid, vals, context=None):
        v_name = None
        v_firstname = None

        if vals.get('name'):
            v_firstname = vals['name'].strip()
            vals['name'] = v_firstname.title()
                           
        
        result = super(communes, self).create(cr, uid, vals, context=context)
        return result
    
    _columns = {
        'name':fields.char('Commune'),
                }
     
communes()

class paroisse_ceb(osv.osv):
    _name = "paroisse.ceb"
    
    _columns = {
        'name':fields.char('CEB'),
                }
     
paroisse_ceb()
class paroisse_paroissien(osv.Model):
    
    _inherit = 'res.partner'
   
    def _get_paroisse_age(self, cr, uid, ids, name, arg, context={}):
        def compute_age_from_dates (patient_dob):
            now = DateTime.now()
            if (patient_dob):
                dob = DateTime.strptime(patient_dob, '%Y-%m-%d')                
                delta = DateTime.Age (now, dob)               
                years_months_days = str(delta.years)
            else:
                years_months_days = "No DoB !"          
 
            return years_months_days
        result = {}
        for patient_data in self.browse(cr, uid, ids, context=context):
                result[patient_data.id] = compute_age_from_dates (patient_data.date_naissance)
        return result
    
    def _get_name_complet(self, cr, uid, ids, field, arg, context=None):
        res = {} 
        reads = self.read(cr, uid, ids, ['name', 'prenom', 'prenom1', 'prenom2'], context=context)        
        for record in reads:
            name = record['name']
            if record['prenom']:
                name = name + " " + record['prenom']
            if record['prenom1']:
                name = name + " " + record['prenom1']
            if  record['prenom2']:
                name = name + " " + record['prenom2']                   
            res[record['id']] = name
        return res

 # Moi debut
 #    def _get_name_complet2(self, cr, uid, ids,field, arg, context=None):
 #        res = {}
 #        reads = self.read(cr, uid, ids, ['name','prenom','prenom1','prenom2'], context=context)
 #        for record in reads:
 #            name = record['prenom']
 #            if record['prenom1']:
 #                name = name +" "+record['prenom1']
 #            if record['prenom2']:
 #                name = name +" "+record['prenom2']
 #            if  record['name']:
 #                name = name +" "+record['name']
 #            res[record['id']] = name
 #        return res

    def _get_name_complet2(self, cr, uid, ids, field, arg, context=None):
        res = {}
        reads = self.read(cr, uid, ids, ['name', 'prenom', 'prenom1', 'prenom2'], context=context)
        for record in reads:
            name = record['name']
            if record['prenom']:
                name = name + " " + record['prenom']
            if record['prenom1']:
                name = name + " " + record['prenom1']
            if  record['prenom2']:
                name = name + " " + record['prenom2']
            res[record['id']] = name
        return res

   # Fin Moi

    _sql_constraints = [
    ('paroisse_uniq', 'unique (name,prenom,prenom1,prenom2,date_naissance)', 'Paroissien doit etre unique !'),
    ]


    def name_search(self, cr, uid, name, args=None, operator='ilike', context=None, limit=100):   
        if context.get('form_view_ref') and (context['form_view_ref'] == 'Gestion_paroisse.paroissien_form_view' 
                                             or context['form_view_ref'] == 'Gestion_paroisse.paroissien_form_view'):
            args.append(['is_maried', '=', False])
        result = super(paroisse_paroissien, self).name_search(cr, uid, name, args, operator=operator, context=context, limit=limit)
        return result
    
    def name_get(self, cr, uid, ids, context=None):
        if isinstance(ids, (list, tuple)) and not len(ids):
            return []
        
        if isinstance(ids, (long, int)):
            ids = [ids]
         
        reads = self.read(cr, uid, ids, ['name', 'prenom', 'prenom1', 'prenom2', 'is_maried'], context=context)
        res = []
        for record in reads :
#             if not record['is_maried']:
                name = record['name']
                if record['prenom']:
                    name = name + " " + record['prenom']
                if record['prenom1']:
                    name = name + " " + record['prenom1']
                if  record['prenom2']:
                    name = name + " " + record['prenom2']    
                res.append((record['id'], name))
        return res
    
    _sql_constraints = [('paroissien_uniq', 'unique(prenom,prenom1,prenom2,name,date_naissance)', 'Un Paroissien doit etre unique  !') ] 
    """
    def _check_unique_paroissien(self, cr, uid, ids): 
        current_record = self.browse(cr,uid,ids[0],{})
        ids =False
        name = current_record.name
        prenom = current_record.prenom
        prenom1 = current_record.prenom1
        prenom2 = current_record.prenom2
        date_naissance = current_record.date_naissance        
        if  prenom1 == False and prenom2 == False:
            ids = self.search(cr, uid, [('name','=',name),('prenom','=',prenom),('date_naissance','=',date_naissance)],{})
        if  prenom1  and prenom2 == False:
            ids = self.search(cr, uid, [('name','=',name),
                                        ('prenom','=',prenom),
                                        ('date_naissance','=',date_naissance),
                                        ('prenom1','=',prenom1)],{})
        if  prenom1 == False  and prenom2:
            ids = self.search(cr, uid, [('name','=',name),
                                        ('prenom','=',prenom),
                                        ('date_naissance','=',date_naissance),
                                        ('prenom2','=',prenom2)],{})
            
        if  prenom1  and prenom2:
            ids = self.search(cr, uid, [('name','=',name),
                                        ('prenom','=',prenom),
                                        ('date_naissance','=',date_naissance),
                                        ('prenom2','=',prenom2),('prenom1','=',prenom1)],{})
        if len(ids)== 1:
            return True
        else :
            return False
             
                
        return True"""

# Test
    def onchange_prenom(self, cr, uid, ids, prenom2, context=None):
        return "good"
         # res = {}
        #  zones = self.pool.get('paroisse.je_soussigne').browse(cr,uid,ids,context=context)
        #  for zone in zones:
        #     res['test'] = zone.boite_postale
        #     return {'value':res}
      #  if lieu_residence:
      #    for zone in zones:
      #       res[zone.id] = zones.name
      #    return {'value':{'test': zones.boite_postale}}
      #       return  res


# This function automatically sets the currency to EUR.
# Pour Verif
#     def _get_default_diocese_id(self, cr, uid, context=None):
#         res = self.pool.get('paroisse.je_soussigne').search(cr, uid, [('name','=','Archidiocese du Dakar')], context=context)
#         return res and res[0] or False
#
#     def _get_default_type_engagement_id(self, cr, uid, context=None):
#         res = self.pool.get('paroisse.typ_engagement_religieux').search(cr, uid, [('name','=','cardinalat')], context=context)
#         return res and res[0] or False
#
#     def _get_default_communes_id(self, cr, uid, context=None):
#         res = self.pool.get('res.communes').search(cr, uid, [('name','=','Dakar-Plateau')], context=context)
#         return res and res[0] or False


# Fin Test
    def write(self, cr, uid, ids, vals, context=None):
        v_name = None
        v_firstname = None

        if vals.get('is_paroissien'):
            # if vals.get("name"):
            # name to Uppercase
            v_firstname = vals['name'].strip()
            vals['name'] = v_firstname.upper()
            
        if vals.get('prenom'):
            # firstname capitalized
            v_firstname = vals['prenom'].strip()
            vals['prenom'] = v_firstname.title()
            
        if vals.get('prenom1'):
            # firstname capitalized
            v_firstname = vals['prenom1'].strip()
            vals['prenom1'] = v_firstname.title()
        if vals.get('prenom2'):
            # firstname capitalized
            v_firstname = vals['prenom2'].strip()
            vals['prenom2'] = v_firstname.title() 
           
        if vals.get('pere'):
            v_firstname = vals['pere'].strip()
            vals['pere'] = v_firstname.title()  
        if vals.get('prenom_pere'):
            v_firstname = vals['prenom_pere'].strip()
            vals['prenom_pere'] = v_firstname.title() 
            
        if vals.get('mere'):
            v_firstname = vals['mere'].strip()
            vals['mere'] = v_firstname.title()  
        if vals.get('prenom_mere'):
            v_firstname = vals['prenom_mere'].strip()
            vals['prenom_mere'] = v_firstname.title()      
            
        if vals.get('parain'):
            v_firstname = vals['parain'].strip()
            vals['parain'] = v_firstname.title()  
        if vals.get('parain_prenom'):
            v_firstname = vals['parain_prenom'].strip()
            vals['parain_prenom'] = v_firstname.title()        
            
        if vals.get('maraine'):
            v_firstname = vals['maraine'].strip()
            vals['maraine'] = v_firstname.title()  
        if vals.get('maraine_prenom'):
            v_firstname = vals['maraine_prenom'].strip()
            vals['maraine_prenom'] = v_firstname.title()      
            
        if vals.get('diocese_bapteme'):
            vals['diocese_bapteme'] = vals['diocese_bapteme'].title()
        
        if vals.get('diocese_confirmation'):
            vals['diocese_confirmation'] = vals['diocese_confirmation'].title()
    
        if vals.get('lieu_naissance'):
            vals['lieu_naissance'] = vals['lieu_naissance'].title()        
            
        if vals.get('pretre_id'):
            vals['pretre_id'] = vals['pretre_id'].title()
        
        if vals.get('pretre_confirmation_id'):
            vals['pretre_confirmation_id'] = vals['pretre_confirmation_id'].title()     

        if vals.get('adresse_parent'):
            vals['adresse_parent'] = vals['adresse_parent'].title()  
            
        if vals.get('ville'):
            vals['ville'] = vals['ville'].title()  
            
        if vals.get('street'):
            vals['street'] = vals['street'].title()      
        if vals.get('civ_religieuse', False) == 'decede':
            mariage_ids = self.pool.get('paroisse.mariage').search(cr, uid, ['|', ('marie_id', '=', ids), ('mari_id', '=', ids)])
            for mariage_id in mariage_ids:
                mariage = self.pool.get('paroisse.mariage').browse(cr, uid, mariage_id)
                cr.execute("update res_partner set is_maried ='f' and civ_religieuse ='conjoint_decedes' where id in " + str((mariage.mari_id.id, mariage.marie_id.id)))

        result = super(paroisse_paroissien, self).write(cr, uid, ids, vals, context=context)
        return result

    def create(self, cr, uid, vals, context=None):
        v_name = None
        v_firstname = None

        if vals.get('is_paroissien'):
 #       if vals.get('name'):
            # name to Uppercase
            v_firstname = vals['name'].strip()
            vals['name'] = v_firstname.upper()
 
        if vals.get('prenom'):
            # firstname capitalized
            v_firstname = vals['prenom'].strip()
            vals['prenom'] = v_firstname.title()

        if vals.get('prenom1'):
            # firstname capitalized
            v_firstname = vals['prenom1'].strip()
            vals['prenom1'] = v_firstname.title()

        if vals.get('prenom2'):
            # firstname capitalized
            v_firstname = vals['prenom2'].strip()
            vals['prenom2'] = v_firstname.title()
            
        if vals.get('pere'):
            v_firstname = vals['pere'].strip()
            vals['pere'] = v_firstname.title()  
        if vals.get('prenom_pere'):
            v_firstname = vals['prenom_pere'].strip()
            vals['prenom_pere'] = v_firstname.title()  
        
        if vals.get('mere'):
            v_firstname = vals['mere'].strip()
            vals['mere'] = v_firstname.title()  
        if vals.get('prenom_mere'):
            v_firstname = vals['prenom_mere'].strip()
            vals['prenom_mere'] = v_firstname.title()  
            
        if vals.get('parain'):
            v_firstname = vals['parain'].strip()
            vals['parain'] = v_firstname.title()  
        if vals.get('parain_prenom'):
            v_firstname = vals['parain_prenom'].strip()
            vals['parain_prenom'] = v_firstname.title()        
            
        if vals.get('maraine'):
            v_firstname = vals['maraine'].strip()
            vals['maraine'] = v_firstname.title()  
        if vals.get('maraine_prenom'):
            v_firstname = vals['maraine_prenom'].strip()
            vals['maraine_prenom'] = v_firstname.title()          
            
        if vals.get('lieu_naissance'):
            vals['lieu_naissance'] = vals['lieu_naissance'].title()    
            
        if vals.get('diocese_bapteme'):
            vals['diocese_bapteme'] = vals['diocese_bapteme'].title()
        
        if vals.get('diocese_confirmation'):
            vals['diocese_confirmation'] = vals['diocese_confirmation'].title()

        if vals.get('pretre_id'):
            vals['pretre_id'] = vals['pretre_id'].title()
         
        if vals.get('pretre_confirmation_id'):
            vals['pretre_confirmation_id'] = vals['pretre_confirmation_id'].title()  
            
        if vals.get('adresse_parent'):
            vals['adresse_parent'] = vals['adresse_parent'].title()
            
        if vals.get('ville'):
            vals['ville'] = vals['ville'].title()    
            
        if vals.get('street'):
            vals['street'] = vals['street'].title()  
           
        result = super(paroisse_paroissien, self).create(cr, uid, vals, context=context)
        return result
   

    _columns = {
        # 'nom':fields.char('Nom',required=True),

            'is_paroissien':fields.boolean('Paroissien'),
            'date_naissance':fields.date('Date naissance'),
            'prenom':fields.char('Prénom', required=True),
            'prenom1':fields.char('Prénom'),
            'prenom2':fields.char('Prénom'),
            'lieu_naissance':fields.char('Lieu de naissance'),
            'pere':fields.char('Nom du père'),
            'prenom_pere':fields.char('Prénom du père'),
            'mere':fields.char('Nom de la mère'),
            'prenom_mere':fields.char('Nom de la mère'),
            'adresse_parent':fields.char('Adresse parent'),
            # 'pretre_id':fields.many2one('paroisse.pretre','Célébré par'),
            'pretre_id':fields.char('Célébré par'),
            'date_bapteme':fields.date('Date baptème', required=False),
            'num_registre_seq':fields.char('Numéro de registre sequentiel'),
            'num_registre':fields.char('N° registre', required=False),
            'parain':fields.char('Nom '),
            'maraine':fields.char('Nom'),
            'parain_prenom':fields.char('Prénom'),
            'maraine_prenom':fields.char('Prénom'),
            'represente_parain_par':fields.char('Représenté par'),
            'represente_maraine_par':fields.char('Représentée par'),
            'sexe':fields.selection([
                ('m', 'Masculin'),
                ('f', 'Féminin'),
                ], 'Status', required=True),
            'civilite':fields.selection([
                ('monsieur', 'Monsieur'),
                ('madame', 'Madame'),
                 ], 'Civilité'),
            'civ_religieuse':fields.selection([
                    ('demande_bapt', 'Demande Baptème'),
                    ('bapt', 'Baptème'),
                    ('conf', 'Confirmation'),
                    ('prof_foi', 'Profession de foi'),
                    ('mariage', 'Mariage'),
                    ('conjoint_decede', 'Conjoint(e) décédé(e)'),
                    ('decede', 'Disparition'),
                     ], 'Etat religieux', select=True),
           # 'conf_id':fields.many2one('paroisse.confirmation','Confirmation',ondelete="cascade"),
          #  'name_template': fields.related('conf_id', 'name', string="Template Name", type='char', size=128, store=True, select=True),
            'date_conf':fields.date('Date confirmation',),
            'pretre_confirmation_id':fields.char('Célébré par'),
            'company_conf_id':fields.many2one('res.company', 'A Eglise'),
            'parent_company_conf_id':fields.many2one('res.company', 'Diocèse de'),

            
            'is_maried':fields.boolean('Is maried'),
            'date_prof':fields.date('Date profession',),
            'pretre_prof_id':fields.char('Célébré par'),
            'company_prof_id':fields.many2one('res.company', 'A Eglise'),
            'parent_company_prof_id':fields.many2one('res.company', 'Diocèse de'),

            'company_bapteme_id':fields.many2one('res.company', 'A Eglise'),
            'parent_company_bapteme_id':fields.many2one('res.company', 'Diocèse de'),

        # Pour Qweb ----------------------------------------------------------------
        #     'diocese3_paroisse': fields.char('Diocese saisissant'),
        #     'lieu3_presbytere': fields.char('Lieu presbytère'),
        #     'paroisse3_saisissant_id': fields.many2one('paroisse.je_soussigne3'),
        # ----------------------------------------------------------------------------

        'mariages_ids':fields.one2many('paroisse.mariage', 'mari_id', 'Mariage', required=False),
            'mariages1_ids':fields.one2many('paroisse.mariage', 'marie_id', 'Mariage', required=False),
            'name_complet': fields.function(_get_name_complet, method=True, type='char', size=100, string='Paroissien', store=True),
            'age': fields.function(_get_paroisse_age, method=True, type='char', size=60, string='Age',),
            'event_ids':fields.one2many('event.event', 'paroissien_id', 'Liste des évènements',),
            'partici_ids':fields.one2many('event.registration', 'partner_id', 'Liste des évènements',),
            'lieu_deces':fields.char('Lieu de décès'),
            'date_deces':fields.date('Date de décès', required=False),
            'street':fields.char('Adresse', size=64, required=False, readonly=False),
            'ville':fields.char('Ville', size=64, required=False, readonly=False),
            'commune_id': fields.many2one('res.communes', string='Commune'),
            'ceb_id':fields.many2one('paroisse.ceb', 'CEB'),
            'actif':fields.boolean('Active'),
            'is_information_marginale':fields.boolean('Informations marginales'),
            'is_engagement_religieux':fields.boolean('Statut religieux'),
            'pretre_id':fields.char('Célébré par'),
            'comp_id':fields.many2one('res.company', 'Eglise', required=True),
            'parent_comp_id':fields.many2one('res.company', 'Diocèse', required=True),
            # 'name_complet': fields.function(_get_name_complet, method=True,
            #                                 type='char',size=100, string='Paroissien',store=True),
           'information_marginale':fields.text('Informations marginales'),
           'engagement_religieux_ids':fields.one2many('paroisse.engagement_religieux', 'paroissien_id',
                                                   'Liste des statut religieux', required=True),
# Moi
           'dispense_obtenue':fields.text('Dispenses obtenues'),
           'liste_dispense_ids':fields.one2many('paroisse.dispense_obtenue', 'dispense_id',
                                                      'Liste des dispenses obtenues'),

           'accord_obtenu_f': fields.boolean('Accord obtenu'),
           'accord_obtenu_m': fields.boolean('Accord obtenu'),
           'is_remaried':fields.boolean('Est remarié', readonly=True),
           'demande_remariage':fields.boolean('Demande remariage'),
           'date_autorisation': fields.date('Date autorisation'),
           'autorisation': fields.char('Autorisation'),
           'par_eveque': fields.char('Par l\'évêque'),

           'is_dispense': fields.boolean('Dispenses_obtenues'),
           'date_dispense': fields.date('Date obtention dispense'),
           'par_eveque_dispense': fields.char('Par l\'Evêque'),
           'name_complet2': fields.function(_get_name_complet2, method=True,
                                            type='char', size=100, string='Paroissien', store=True),
            # 'je_soussigne_id': fields.many2one('paroisse.je_soussigne', 'Je soussigne'),
        # 'lieu_presbytere': fields.related('je_soussigne_id', 'lieu_presbytere', type='char', string='Lieu du presbytère'),
        #     'lieu_presbytere':fields.related('je_soussigne_id','lieu_presbytere', type='char',
        #                             relation="paroisse.je_soussigne" , string='Lieu du presbytère'),
        #     'lieu_presbytere2':fields.related('je_soussigne_id','lieu_presbytere', type='char',
        #                             relation="paroisse.je_soussigne" , string='Lieu du presbytère'),
        # Fin Moi
# Moi 27-04
#     Pour accès aux paramètres de configuration
        'diocese_id': fields.many2one('paroisse.je_soussigne','Diocèse de l\'utilisateur'),
        # 'diocese_id': fields.many2one('paroisse.je_soussigne', default='Archidiocèse de Dakar', string='Diocèse de l\'utilisateur'),

        # 'test': fields.char('Test'),
        # 'type_engagement_id': fields.many2one('paroisse.typ_engagement_religieux', 'Engagement Religieux'),

        'communes_id': fields.many2one('res.communes', 'Communes'),
	
# Le 29/05/2016
#'date_saisie':fields.date('Date de saisie'),
	'date_saisie': fields.datetime('Date de saisie'),
#	'heure_saisie': fields.datetime('Date saisie'),
	'createur_record_id': fields.many2one('res.users', string='Créateur'),

# Zones de réserve
	'zone1': fields.char('Attente utilisation'),
	'zone2': fields.integer('En attente utilisation'),
	'zone3': fields.char('Attente utilisation'),
	'zone4': fields.text('Attente utilisation'),
    }
    
    _defaults = { 'civ_religieuse':'demande_bapt',
                 'company_id':1,
#		'date_saisie': fields.date.context_today,
		'date_saisie': lambda *a: time.strftime('%Y-%m-%d %H:%M:%S'),
#		'heure_saisie': lambda *a: utc_today().strftime('%H:%M:%S'),
		'createur_record_id': lambda sel,cr,uid, ctx=None:uid,
                'company_id': lambda self, cr, uid, ctx: self.pool.get('res.company')._company_default_get(cr, uid, 'res.partner', context=ctx),

       # Pour Verif
                 # 'type_engagement_id': _get_default_type_engagement_id,
            # Pour Verif
                 # 'diocese_id': _get_default_diocese_id,
                # 'communes_id': _get_default_communes_id
                  }
    
paroisse_paroissien()

# Moi

class dispense_obtenue(osv.osv):
    _name = "paroisse.dispense_obtenue"

    _columns = {
        'name':fields.char('De l\'évèque'),
        'typ_dispense_id': fields.many2one('paroisse.typ_dispense',
                                              string='Nature dispense'),
        'date_dispense':fields.date('Le:'),
        'dispense_id':fields.many2one('res.partner', 'Dispense')
                }
dispense_obtenue()


class typ_dispense(osv.osv):
    _name = "paroisse.typ_dispense"

    _columns = {
              'name':fields.char('Type dispense'),

                }

typ_dispense()

class je_soussigne(osv.osv):
    _name = "paroisse.je_soussigne"

    _columns = {
    # Pour verif
        # 'name': fields.many2one('paroisse.dioceses_senegal', 'Diocèse de la paroisse'),
        'name': fields.char('Diocèse paroisse'),
        'nom_paroisse': fields.char('Nom de la paroisse'),
        'nom_paroisse2': fields.char('Zone test paroisse'),
        'adresse_paroisse': fields.char('Adresse'),
        'boite_postale': fields.char('Boite postale'),
        'telephone_paroisse': fields.char('Téléphone'),
        'email_paroisse': fields.char('Email'),
        'lieu_presbytere': fields.char('Lieu presbytère'),
        'zone_je_soussigne': fields.char('Zone je soussigné'),
        'zone_distante2': fields.char('Zone distante'),

    }
je_soussigne()

# Copie du précédant pour gerer Qweb Version-3
# class je_soussigne3(osv.osv):
#     _name = "paroisse.je_soussigne3"
#
#     _columns = {
#         'name3_id': fields.char('Paroisse'),
#         'name': fields.many2one('res.company', string='Paroisse du saisissant'),
#         'diocese_paroisse3': fields.char('Diocèse du saisissant'),
#         'adresse_paroisse3': fields.char('Adresse'),
#         'boite_postale3': fields.char('Boite postale'),
#         'telephone_paroisse3': fields.char('Téléphone'),
#         'email_paroisse3': fields.char('Email'),
#         'lieu_presbytere3': fields.char('Lieu presbytère'),
#         'zone_je_soussigne3': fields.char('Zone je soussigné'),
#     }
# je_soussigne3()


class dioceses_senegal(osv.osv):
    _name = "paroisse.dioceses_senegal"

    _columns = {
        'name': fields.char('Diocèses'),

    }

dioceses_senegal()

# Fin Moi

class engagement_religieux(osv.osv):
    _name = "paroisse.engagement_religieux"
    _order = "date desc"
    
    def write(self, cr, uid, ids, vals, context=None):
        v_name = None
        v_firstname = None

        if vals.get('name'):
            vals['name'] = vals['name'].title() 
     
        if vals.get('diocese_engagement'):
            vals['diocese_engagement'] = vals['diocese_engagement'].title()  
            
        result = super(engagement_religieux, self).write(cr, uid, ids, vals, context=context)
        return result

    def create(self, cr, uid, vals, context=None):
        v_name = None
        v_firstname = None

        if vals.get('name'):
            vals['name'] = vals['name'].title()   
            
        if vals.get('diocese_engagement'):
            vals['diocese_engagement'] = vals['diocese_engagement'].title()  
        
        result = super(engagement_religieux, self).create(cr, uid, vals, context=context)
        return result
   
    
    _columns = {
        'name':fields.char('A reçu l\'ordination  des mains de', required=True),
        'date':fields.date('Le:', required=True),
        'date_fin':fields.date('Au:'),
        'typ_engagement_id': fields.many2one('paroisse.typ_engagement_religieux',
                                              string='Engagement religieux', required=True),
        'a':fields.char('A',),
        'en_eglise_id':fields.many2one('res.company', 'En l\'église de', required=True),
        'parent_en_eglise_id':fields.many2one('res.company', 'Diocèse de', required=True),

        'paroissien_id':fields.many2one('res.partner', 'Paroissien', required=True)      
                }
engagement_religieux()

class typ_engagement_religieux(osv.osv):
    _name = "paroisse.typ_engagement_religieux"
    
    def write(self, cr, uid, ids, vals, context=None):
        v_name = None
        v_firstname = None

        if vals.get('name'):
            vals['name'] = vals['name'].title() 
            
        result = super(typ_engagement_religieux, self).write(cr, uid, ids, vals, context=context)
        return result

    def create(self, cr, uid, vals, context=None):
        v_name = None
        v_firstname = None

        if vals.get('name'):
            vals['name'] = vals['name'].title()   
        
        result = super(typ_engagement_religieux, self).create(cr, uid, vals, context=context)
        return result
   
    
    _columns = {
              'name':fields.char('Engagement religieux', required=True),

                }
     
typ_engagement_religieux()



