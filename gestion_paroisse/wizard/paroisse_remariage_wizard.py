# -*- coding: utf-8 -*-
from openerp.osv  import osv,fields

class decede(osv.TransientModel):

    _name = "paroisse.remariage.wizard"


    _columns = {

        'autorisation_wizard':fields.char('Autorisation'),
        'par_eveque_wizard': fields.char('Par évêque'),
        'par_id':fields.integer('id'),
        'date_autorisation_wizard':fields.date('Date autorisation',required=False),
                }

    def action_demande(self, cr, uid, ids, context=None):
         values ={}
         wizard = self.browse(cr,uid,ids[0],context)
         id_paroissien = wizard.par_id
         # paroissien = self.pool.get("res.partner").browse(cr,uid,id_paroissien,context)
         # if paroissien.sexe =='f':
         #     for m in paroissien.mariages1_ids:
         #         self.pool.get("res.partner").write(cr,uid,m.mari_id.id,{'is_maried':False,'civ_religieuse':'bapt'})
         # else:
         #     for m in paroissien.mariages_ids:
         #         self.pool.get("res.partner").write(cr,uid,m.marie_id.id,{'is_maried':False,'civ_religieuse':'bapt'})

        # id_paroissien = wizard.par_id
         paroissien = self.pool.get("res.partner").browse(cr,uid,id_paroissien,context)
         if paroissien.sexe =='f':
             for m in paroissien.mariages1_ids:
                 self.pool.get("res.partner").write(cr,uid,m.mari_id.id,{'is_maried':False,'civ_religieuse':'bapt'})
             values['accord_obtenu_m'] = 'True'

         else:
             for m in paroissien.mariages_ids:
                 self.pool.get("res.partner").write(cr,uid,m.marie_id.id,{'is_maried':False,'civ_religieuse':'bapt'})
             values['accord_obtenu_f'] = 'True'



         values['is_remaried'] = 'True'
         # values['accord_obtenu'] = 'True'
         if wizard.date_autorisation_wizard :
             values['date_autorisation'] = wizard.date_autorisation_wizard
             # reads = self.pool.get("paroisse.mariage").read(cr, uid, ids, ['name','prenom','prenom1','prenom2'], context=context)
         if wizard.autorisation_wizard :
             values['autorisation'] = wizard.autorisation_wizard
         if wizard.par_eveque_wizard :
             values['par_eveque'] = wizard.par_eveque_wizard

         self.pool.get("res.partner").write(cr,uid,id_paroissien,values)
         return  {'type':'ir.actions.act_window_close'}

decede()
