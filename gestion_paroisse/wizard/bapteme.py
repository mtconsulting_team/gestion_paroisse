# -*- coding: utf-8 -*-
from openerp.osv  import osv,fields
import pdb

class bapteme(osv.TransientModel):
    
   _name = "paroisse.bapteme.wizard"
   # _defaults = {
  #      'company_id': lambda self, cr, uid, ctx: self.pool.get('res.company')._company_default_get(cr, uid, 'res.partner', context=ctx),
  #      'paroisse3_user_id': lambda self, cr, uid, ctx: self.pool.get('res.company')._company_default_get(cr, uid, 'res.partner', context=ctx),

       # }

   _columns = {
           
            'pere':fields.char('Nom du père'),
            'par_id':fields.integer('id'),
            'prenom_pere':fields.char('Prénom du père'),
            'mere':fields.char('Nom de la mère'),
            'prenom_mere':fields.char('Nom de la mère'),
            'adresse_parent':fields.char('Adresse parent'),            
            #'pretre_id':fields.many2one('paroisse.pretre','Célébré par'),
            'pretre_id':fields.char('Célébré par'),
            'date_bapteme':fields.date('Date baptème',required=True),
            'num_registre':fields.char('Numéro de registre'),
            'parain':fields.char('Nom '),
            'maraine':fields.char('Nom'),
            'parain_prenom':fields.char('Prénom'),
            'maraine_prenom':fields.char('Prénom'),
            'represente_parain_par':fields.char('Représenté par'),
            'represente_maraine_par':fields.char('Représentée par'),
            'company_id':fields.many2one('res.company','Eglise', required=False),
            'parent_company2_id':fields.many2one('res.company','Diocèse', required=False),

        # Pour Qweb
       # 'diocese3_paroisse': fields.char('Diocèse du saisissant'),
    # Field pour chainer dans le fichier de configuration
    #    'nom3_paroisse_id': fields.many2one('paroisse.je_soussigne3', 'Paroisse pour le chainage'),
    #
    #    'lieu3_presbytere': fields.char('Lieu Presbytere'),
    #    'adresse3_paroisse': fields.char('Adresse paroissse'),

    # field récupérant la paroisse du user
    #    'paroisse3_user_id': fields.many2one('res.company', 'Paroisse du user qui se connecte'),

    }

   # Gestion QWEB -------------------------------------------------------------------------
   # --------------------------------------------------------------------------------------
# EXPLICATIONS DES INTENTIONS
#    Pour la gestion des impressions en dynamique:
#    Il y a un fichier de configuration qui s appelle paroisse.je_soussigne3 dans paroissien.py. Chaque paroisse y a
#    un renregistrement avec son nom, son diocèse, son adresse, sa boite postale etc...
#    Ces fields dynamiques sont positionnés avec la synthaxe o.field dans les états Qweb report_gestion_bapteme3.xml
#    et report_mariage3.xml.
#    Lorsqu un user se connecte pour sasir une fiche de bapteme ou un mariage, les zones dynamiques doivent être
#    récupérées automatiquement dans les formulaires de saisies sous separator Infos pour Qweb. Pour obtenir ce
#    remplissage automatique, le programme utilise un onchange sur les nom mari_id et marie_id pour les mariages
#    et date_bapteme pour le baptemes.
#    Lorsqu un user commence sa saisie, la zone paroisse3_user_id many2one vers res.company contient sa paroisse.
#    Par contre comme il faut un champ many2one pour chainer dans paroisse.je_soussigne3 pour récupérer
#    les champs dynamiques,
#    il reste à trouver comment copier le contenu de paroisse3_user_id dans nom3_paroisse_id pour faire le
#    browwse avec nom3_paroisse_id.
#    Tout se déroule normalement lorsqu on sélectionne manuellement la paroisse du user dans la zone
#    nom3_paroisse_id.
#    C est la meme idée pour les mariages


   # def onchange_date_bapteme(self, cr, uid, ids, date_bapteme, paroisse3_user_id, nom3_paroisse_id, context=None):
    # Recuperation paroisse3_user_id
    #     res1 = {}
        # pdb.set_trace()
        # obj1 = self.pool.get('paroisse.bapteme.wizard')
        # current_record = obj1.browse(cr,uid,ids,context)
        # res1['nom3_paroisse_id'] = current_record.paroisse3_user_id.id
        # pdb.set_trace()

    # DOUTES ICI !!!!!!
        # nom3_paroisse_id = paroisse3_user_id

        # res = {}
        # obj = self.pool.get('paroisse.je_soussigne3')
        # zones = obj.browse(cr,uid,nom3_paroisse_id,context=context)
        # res['diocese3_paroisse'] = zones.diocese_paroisse3
        # res['lieu3_presbytere'] = zones.lieu_presbytere3
        # res['nom3_paroisse_id'] = zones.name.id
        # res['adresse3_paroisse']= zones.adresse_paroisse3
        #
        # return {'value': res}


     
   def action_bapteme(self, cr, uid, ids, context=None):
         wizard = self.browse(cr,uid,ids[0],context)
         id_paroissien = wizard.par_id
         values ={}
         values['civ_religieuse']='bapt'
         if wizard.date_bapteme :
             values['date_bapteme'] = wizard.date_bapteme

# Pour Qweb ---------------------------------------------
#          if wizard.diocese3_paroisse :
#               values['diocese3_paroisse'] = wizard.diocese3_paroisse
#
#          if wizard.lieu3_presbytere:
#              values['lieu3_presbytere'] = wizard.lieu3_presbytere

         # if nom3_saisissant_id :
         # values['paroisse3_saisissant_id'] = wizard.nom3_paroisse_id.id
 # ---------------------------------------

         if wizard.pere :
             values['pere'] =wizard.pere
             
         if wizard.prenom_pere :
             values['prenom_pere'] =wizard.prenom_pere 
             
         if wizard.mere :
             values['mere'] =wizard.mere 
         
         if wizard.prenom_mere :
             values['prenom_mere'] =wizard.prenom_mere 
             
         if wizard.adresse_parent :
             values['adresse_parent'] =wizard.adresse_parent     
               
         if wizard.pretre_id :
             values['pretre_id'] =wizard.pretre_id 
             
         if wizard.date_bapteme :
             values['date_bapteme'] =wizard.date_bapteme  
                  
         if wizard.num_registre :
             values['num_registre'] =wizard.num_registre
              
         if wizard.date_bapteme :
             values['date_bapteme'] =wizard.date_bapteme 
             
         if wizard.parain :
             values['parain'] =wizard.parain 
             
         if wizard.maraine :
             values['maraine'] =wizard.maraine 
             
         if wizard.parain_prenom :
             values['parain_prenom'] =wizard.parain_prenom 
             
         if wizard.maraine_prenom :
             values['maraine_prenom'] =wizard.maraine_prenom 
             
         if wizard.represente_parain_par :
             values['represente_parain_par'] =wizard.represente_parain_par 
        
         if wizard.represente_maraine_par :
             values['represente_maraine_par'] =wizard.represente_maraine_par 
             
         if wizard.company_id :
             values['company_bapteme_id'] =wizard.company_id.id
             
         if wizard.parent_company2_id :
              values['parent_company_bapteme_id'] =wizard.parent_company2_id.id       
      
         self.pool.get("res.partner").write(cr,uid,id_paroissien,values)   
         return  {'type':'ir.actions.act_window_close'}

bapteme()


class confirm(osv.TransientModel):
    
    _name = "paroisse.confirm.wizard"
 #   _defaults = {  
 #       'company_conf_id': lambda self, cr, uid, ctx: self.pool.get('res.company')._company_default_get(cr, uid, 'res.partner', context=ctx),
        
 #       }

    _columns = {
            'company_conf_id':fields.many2one('res.company','Eglise', required=False),
            'parent_company_conf_id':fields.many2one('res.company','Diocèse', required=False),
            'date_conf':fields.date('Date confirmation', required=True),
            'pretre_confirmation_id':fields.char('Célébré par'),
            'par_id':fields.integer('id'),

                }
     
    def action_conf(self, cr, uid, ids, context=None):        
         wizard = self.browse(cr,uid,ids[0],context)
         id_paroissien = wizard.par_id
         values ={}
         values['civ_religieuse']='conf'
         if wizard.date_conf :
             values['date_conf'] = wizard.date_conf             
                     
         if wizard.pretre_confirmation_id :
             values['pretre_confirmation_id'] = wizard.pretre_confirmation_id 
            
         if wizard.company_conf_id :
             values['company_conf_id'] = wizard.company_conf_id.id 
             
         if wizard.parent_company_conf_id :
             values['parent_company_conf_id'] = wizard.parent_company_conf_id.id 
      
         self.pool.get("res.partner").write(cr,uid,id_paroissien,values)   
         return  {'type':'ir.actions.act_window_close'}

confirm()

class prof(osv.TransientModel):
    
    _name = "paroisse.prof.wizard"
    _defaults = {  
        'company_prof_id': lambda self, cr, uid, ctx: self.pool.get('res.company')._company_default_get(cr, uid, 'res.partner', context=ctx),
        
        }

    _columns = {
            'date_prof':fields.date('Date profession',),
            'pretre_prof_id':fields.char('Célébré par'),
            'company_prof_id':fields.many2one('res.company','A Eglise'),
            
            'par_id':fields.integer('id'),


                }
     
    def action_prof(self, cr, uid, ids, context=None):        
         wizard = self.browse(cr,uid,ids[0],context)
         id_paroissien = wizard.par_id
         values ={}
         values['civ_religieuse']='prof_foi'
         if wizard.date_prof :
             values['date_prof'] = wizard.date_prof             
                     
         if wizard.pretre_prof_id :
             values['pretre_prof_id'] = wizard.pretre_prof_id 
            
         if wizard.company_prof_id :
             values['company_prof_id'] = wizard.company_prof_id.id 
      
         self.pool.get("res.partner").write(cr,uid,id_paroissien,values)   
         return  {'type':'ir.actions.act_window_close'}

prof()