# -*- coding: utf-8 -*-
from openerp.osv import fields, osv
from openerp.tools.translate import _
from datetime import datetime
import time
import openerp.addons


class Type_depense( osv.osv ):    
    _name = "gestion.paroisse.type_depense"
    _description = 'Type_depense'
    _get_periodicite=[
                 ('journaliere','journaliere'),
                 ('hebdomadaire','hebdomadaire'),
                 ('mensuelle','mensuelle'),
                 ('trimestrielle','trimestrielle'),
                 ('annuelle','annuelle')
                      ]        
                            
    # Moi
     
    def write(self, cr, uid, ids, vals, context=None):
        v_name = None
        v_firstname = None

        if vals.get('name'):
            v_firstname = vals['name'].strip()
            vals['name'] = v_firstname.title()
                          
        result = super(Type_depense,self).write(cr, uid, ids, vals, context=context)
        return result

    def create(self, cr, uid, vals, context=None):
        v_name = None
        v_firstname = None
        
        if vals.get('name'):
            v_firstname = vals['name'].strip()
            vals['name'] = v_firstname.title()
       
        result = super(Type_depense,self).create(cr, uid, vals, context=context)
        return result
           
    #Moi                        
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
    _columns = {
                'name':fields.char('Description', size=64, required=True, readonly=False),
                #'periodicite':fields.selection(_get_periodicite, 'periodicite', required=True),
                }
    default = {
              }
    _sql_constraints = [     ('name_uniq', 'unique (name)', 'The Name of the Type_depense must be unique !'), ]
  
Type_depense()


class depense( osv.osv ):   
    _name = "gestion.paroisse.depense.recette"
    _description = "Depense paroisse"   
    
    # Moi
     
    def write(self, cr, uid, ids, vals, context=None):
        v_name = None
        v_firstname = None

        if vals.get('description'):
            v_firstname = vals['description'].strip()
            vals['description'] = v_firstname.title()
                          
        result = super(depense,self).write(cr, uid, ids, vals, context=context)
        return result

    def create(self, cr, uid, vals, context=None):
        v_name = None
        v_firstname = None
        
        if vals.get('description'):
            v_firstname = vals['description'].strip()
            vals['description'] = v_firstname.title()
       
        result = super(depense,self).create(cr, uid, vals, context=context)
        return result
           
    #Moi
    
    
    _columns = {
                'description':fields.char('Description', size=64, required=True),
                'type_depense':fields.many2one('gestion.paroisse.type_depense', 'Type depense', ),
                'date_dep':fields.datetime('Date',required=True),
                'credit':fields.integer('Dépense'),
                'type':fields.selection([
                ('depense','B-Depense'),
                ('recette','A-Recette'),               
                   ],'Type'),
                'company_id':fields.many2one('res.company','Eglise')

                }
    
    _defaults = {
    'company_id': lambda self, cr, uid, ctx: self.pool.get('res.company')._company_default_get(cr, uid, 'res.partner', context=ctx),
} 
   
    
    
    def onchange_type_depense(self, cr, uid, ids,type_depense, context=None):
        result={}
        if type_depense:
            type_depense_id = self.pool.get('GESTION_CABINET.type_depense').browse(cr, uid, type_depense)
            result['value'] = {'periodicite':type_depense_id.periodicite}      
        return result     
depense()

