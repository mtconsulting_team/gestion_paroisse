# -*- coding: utf-8 -*-
from openerp.osv  import osv,fields

class bapteme(osv.TransientModel):
    
    _name = "paroisse.bapteme.wizard"
  #  _defaults = {  
  #      'company_id': lambda self, cr, uid, ctx: self.pool.get('res.company')._company_default_get(cr, uid, 'res.partner', context=ctx),

   #     }

    _columns = {
           
            'pere':fields.char('Nom du père', required=True),
            'par_id':fields.integer('id'),
            'prenom_pere':fields.char('Prénom du père', required=True),
            'mere':fields.char('Nom de la mère', required=True),
            'prenom_mere':fields.char('Nom de la mère', required=True),
            'adresse_parent':fields.char('Adresse parent'),            
            #'pretre_id':fields.many2one('paroisse.pretre','Célébré par'),
            'pretre_id':fields.char('Célébré par', required=True),
            'date_bapteme':fields.date('Date baptème',required=True),
            'num_registre':fields.char('Numéro de registre', required=True),
            'parain':fields.char('Nom ', required=True),
            'maraine':fields.char('Nom', required=True),
            'parain_prenom':fields.char('Prénom', required=True),
            'maraine_prenom':fields.char('Prénom', required=True),
            'represente_parain_par':fields.char('Représenté par'),
            'represente_maraine_par':fields.char('Représentée par'),
            'company_id':fields.many2one('res.company','Eglise', required=False),
            'parent_company2_id':fields.many2one('res.company','Diocèse', required=False),
                }
     
    def action_bapteme(self, cr, uid, ids, context=None):        
         wizard = self.browse(cr,uid,ids[0],context)
         id_paroissien = wizard.par_id
         values ={}
         values['civ_religieuse']='bapt'
         if wizard.date_bapteme :
             values['date_bapteme'] =wizard.date_bapteme
             
         if wizard.pere :
             values['pere'] =wizard.pere
             
         if wizard.prenom_pere :
             values['prenom_pere'] =wizard.prenom_pere 
             
         if wizard.mere :
             values['mere'] =wizard.mere 
         
         if wizard.prenom_mere :
             values['prenom_mere'] =wizard.prenom_mere 
             
         if wizard.adresse_parent :
             values['adresse_parent'] =wizard.adresse_parent     
               
         if wizard.pretre_id :
             values['pretre_id'] =wizard.pretre_id 
             
         if wizard.date_bapteme :
             values['date_bapteme'] =wizard.date_bapteme  
                  
         if wizard.num_registre :
             values['num_registre'] =wizard.num_registre
             
             
              
         if wizard.date_bapteme :
             values['date_bapteme'] =wizard.date_bapteme 
             
         if wizard.parain :
             values['parain'] =wizard.parain 
             
         if wizard.maraine :
             values['maraine'] =wizard.maraine 
             
         if wizard.parain_prenom :
             values['parain_prenom'] =wizard.parain_prenom 
             
         if wizard.maraine_prenom :
             values['maraine_prenom'] =wizard.maraine_prenom 
             
         if wizard.represente_parain_par :
             values['represente_parain_par'] =wizard.represente_parain_par 
        
         if wizard.represente_maraine_par :
             values['represente_maraine_par'] =wizard.represente_maraine_par 
             
         if wizard.company_id :
             values['company_bapteme_id'] =wizard.company_id.id
             
         if wizard.parent_company2_id :
              values['parent_company_bapteme_id'] =wizard.parent_company2_id.id       
      
         self.pool.get("res.partner").write(cr,uid,id_paroissien,values)   
         return  {'type':'ir.actions.act_window_close'}

bapteme()


class confirm(osv.TransientModel):
    
    _name = "paroisse.confirm.wizard"
 #   _defaults = {  
 #       'company_conf_id': lambda self, cr, uid, ctx: self.pool.get('res.company')._company_default_get(cr, uid, 'res.partner', context=ctx),
        
 #       }

    _columns = {
            'company_conf_id':fields.many2one('res.company','Eglise', required=False),
            'parent_company_conf_id':fields.many2one('res.company','Diocèse', required=False),
            'date_conf':fields.date('Date confirmation', required=True),
            'pretre_confirmation_id':fields.char('Célébré par'),
            'par_id':fields.integer('id'),

                }
     
    def action_conf(self, cr, uid, ids, context=None):        
         wizard = self.browse(cr,uid,ids[0],context)
         id_paroissien = wizard.par_id
         values ={}
         values['civ_religieuse']='conf'
         if wizard.date_conf :
             values['date_conf'] = wizard.date_conf             
                     
         if wizard.pretre_confirmation_id :
             values['pretre_confirmation_id'] = wizard.pretre_confirmation_id 
            
         if wizard.company_conf_id :
             values['company_conf_id'] = wizard.company_conf_id.id 
             
         if wizard.parent_company_conf_id :
             values['parent_company_conf_id'] = wizard.parent_company_conf_id.id 
      
         self.pool.get("res.partner").write(cr,uid,id_paroissien,values)   
         return  {'type':'ir.actions.act_window_close'}

confirm()

class prof(osv.TransientModel):
    
    _name = "paroisse.prof.wizard"
    _defaults = {  
        'company_prof_id': lambda self, cr, uid, ctx: self.pool.get('res.company')._company_default_get(cr, uid, 'res.partner', context=ctx),
        
        }

    _columns = {
            'date_prof':fields.date('Date profession',),
            'pretre_prof_id':fields.char('Célébré par'),
            'company_prof_id':fields.many2one('res.company','A Eglise'),
            
            'par_id':fields.integer('id'),


                }
     
    def action_prof(self, cr, uid, ids, context=None):        
         wizard = self.browse(cr,uid,ids[0],context)
         id_paroissien = wizard.par_id
         values ={}
         values['civ_religieuse']='prof_foi'
         if wizard.date_prof :
             values['date_prof'] = wizard.date_prof             
                     
         if wizard.pretre_prof_id :
             values['pretre_prof_id'] = wizard.pretre_prof_id 
            
         if wizard.company_prof_id :
             values['company_prof_id'] = wizard.company_prof_id.id 
      
         self.pool.get("res.partner").write(cr,uid,id_paroissien,values)   
         return  {'type':'ir.actions.act_window_close'}

prof()