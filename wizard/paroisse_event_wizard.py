# -*- coding: utf-8 -*-
from openerp.osv import fields, osv


#-------------------------------------------------------------------------------
# report_report_salary
#-------------------------------------------------------------------------------
class report_event_list(osv.osv_memory):
    _name = "event.list"
    
    _columns = {
        'date_begin': fields.date('Date début'),
        'date_end': fields.date('Date fin'),
        'custom_period':fields.boolean('Periode De Choix'),
        }
    
    _defaults = {
    }
    
    
    def print_report(self, cr, uid, ids, context=None):
        datas = {'ids': context.get('active_ids', [])}
        datas['form'] = self.read(cr, uid, ids, context=context)[0]
        return self.pool['report'].get_action(cr, uid, [], 'gestion_paroisse.event_list',data=datas, context=context) 
    
    #---------------------------------------------------------------------------
    # @api.multi  
    # def print_report(self):
    #     datas = {'ids': self.context.get('active_ids', [])}
    #     datas['form'] = self.read(self.cr, self.uid, self.ids, context=self.context)[0]
    #     return self.env['report'].get_action(self, 'project_extend.report_salary',data=datas, context=self.context)
    #     
    #---------------------------------------------------------------------------
report_event_list()

