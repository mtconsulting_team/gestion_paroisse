# -*- coding: utf-8 -*-
from openerp.osv  import osv,fields

class decede(osv.TransientModel):

    _name = "paroisse.decede.wizard"


    _columns = {

            'lieu_deces':fields.char('Lieu de décès'),
            'par_id':fields.integer('id'),
            'date_deces':fields.date('Date de décès',required=False),
                }

    def action_deces(self, cr, uid, ids, context=None):
         wizard = self.browse(cr,uid,ids[0],context)
         id_paroissien = wizard.par_id
         paroissien = self.pool.get("res.partner").browse(cr,uid,id_paroissien,context)
         if paroissien.sexe =='f':
             for m in paroissien.mariages1_ids:
                 self.pool.get("res.partner").write(cr,uid,m.mari_id.id,{'is_maried':False,'civ_religieuse':'bapt'})              
         else:
             for m in paroissien.mariages_ids:
                 self.pool.get("res.partner").write(cr,uid,m.marie_id.id,{'is_maried':False,'civ_religieuse':'bapt'})
                 
         values ={}
         values['civ_religieuse'] = 'decede'
         if wizard.date_deces :
             values['date_deces'] = wizard.date_deces
             reads = self.pool.get("paroisse.mariage").read(cr, uid, ids, ['name','prenom','prenom1','prenom2'], context=context)        
         if wizard.lieu_deces :
             values['lieu_deces'] = wizard.lieu_deces

         self.pool.get("res.partner").write(cr,uid,id_paroissien,values)
         return  {'type':'ir.actions.act_window_close'}

decede()
